@extends('index')

@section('title')
Production Line {{ $productionline->linenumber }}
@endsection

@section('extra-css')
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-tagsinput.css')}}
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Production Line: {{ $productionline->linenumber }}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/productionionlines/') }}" class="btn bg-deep-orange waves-effect" title="Back to Production Lines">
                                <i class="material-icons">reply</i>
                                <span class="sr-only">Back to Production Lines</span>
                            </a>
                        </li>
                    </ul>                   
                </div>
                <div class="body">
                    <div class="body">
                    	<div class="row">
                            <div class="col-sm-6">
                                <label>Line Number</label><br>
                            	{{$productionline->linenumber}}
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Stations</label><br>
                                @if ($productionline->station->count())
                                    @foreach($productionline->station as $station)
                                        <span class="badge bg-blue">{{$station->name}}</span>
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label>Products</label><br>
                                @if ($productionline->products->count())
                                    @foreach($productionline->products as $product)
                                        <span class="badge bg-purple">{{$product->name}}</span>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
@endsection