
@extends('index')

@section('title')
	Create New Production Line
@endsection

@section('extra-css')
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
    {{Html::style('bsbmd/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Add New Production Line
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/productionionlines/') }}" class="btn bg-deep-orange waves-effect" title="Back to Production Line">
                                <i class="material-icons">reply</i>
                                <span class="sr-only">Back to Production Line</span>
                            </a>
                        </li>
                    </ul> 
                </div>
                <div class="body">
                    <div class="body">
                        {!! Form::open(array('action' => 'ProductLinesManagementController@store', 'method' => 'POST', 'role' => 'form', 'id' => 'form_validation_Productionline')) !!}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            {!! Form::text('linenumber', NULL, array('id' => 'linenumber', 'class' => 'form-control','required')) !!}
                                            {!! Form::label('linenumber', 'Line Number' , array('class' => 'form-label')); !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                </div>
                                <div class="col-sm-6">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary waves-effect">
                                <i class="material-icons">save</i>
                                <span>Save New Production Line</span>
                            </button>
                            <a href="{{ url('/productionionlines/') }}" class="btn bg-deep-orange waves-effect" title="Back to Productionion Lines">
                                  <i class="material-icons">reply</i>
                                  <span class="sr-only">Back to Productionion Lines</span>
                              </a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
    {{Html::script('bsbmd/plugins/jquery-validation/jquery.validate.js')}}
    {{Html::script('bsbmd/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form_validation_Productionline').validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });
        } );
    </script>
@endsection