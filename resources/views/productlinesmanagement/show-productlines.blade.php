@extends('index')

@section('title')
    Production Lines List
@endsection

@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css">
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">
        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Production Lines List
                        <small style="display: inline-block;">{{ count($productionionlines) }} Total Production Lines</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/productionionlines/create') }}" class="mdl-button mdl-button--icon bg-deep-orange waves-effect" title="Add New Production Line">
                                <i class="material-icons">add</i>
                                <span class="sr-only">Add New Production Line</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="" class="table table-bordered table-striped table-hover js-basic-example dataTable mdl-data-table data-table-product" style="width:100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Line No</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($productionionlines as $productline)
                                    <tr>
                                        <td><a href="{{ URL::to('productionionlines/' . $productline->id) }}">{{$productline->id}}</a></td>
                                        <td><a href="{{ URL::to('productionionlines/' . $productline->id) }}">{{$productline->linenumber}}</a></td>
                                        <!--<td>
                                            @foreach($productline->station as $station)
                                                <span class="badge bg-blue">{{$station->name}}</span>
                                            @endforeach
                                        </td>-->
                                        <!--<td>
                                            @foreach($productline->products as $product)
                                                <span class="badge bg-purple">{{$product->name}}</span>
                                            @endforeach
                                        </td>-->
                                        <td>
                                            {{-- VIEW PRODUCT ICON BUTTON --}}
                                            <a href="{{ URL::to('productionionlines/' . $productline->id) }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="View Product Detail">
                                                <i class="material-icons mdl-color-text--blue">visibility</i>
                                            </a>

                                            {{-- EDIT PRODUCT ICON BUTTON --}}
                                            <a href="{{ URL::to('productionionlines/' . $productline->id . '/edit') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                                                <i class="material-icons mdl-color-text--orange">edit</i>
                                            </a>

                                            {{-- DELETE ICON BUTTON AND FORM CALL --}}
                                            {!! Form::open(array('url' => 'productionionlines/' . $productline->id, 'class' => 'inline-block', 'id' => 'delete_'.$productline->id)) !!}
                                                {!! Form::hidden('_method', 'DELETE') !!}
                                                <a href="javascript:void(0);" class="dialog-button dialiog-trigger-delete dialiog-trigger{{$productline->id}} mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" data-productid="{{$productline->id}}">
                                                    <i class="material-icons mdl-color-text--red">delete</i>
                                                </a>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Line No</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.material.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.data-table-product').DataTable( {
                order: [[0, 'desc']],
                responsive: true,
                "bLengthChange": false,
                "oLanguage": {
                    "sSearch": "",
                    "sSearchPlaceholder": "Enter Keywords Here",
                },
                columnDefs: [
                {
                        targets: [ 0, 1, 2],
                        className: 'mdl-data-table__cell--non-numeric'
                }
                ]
            });
            $('.dialiog-trigger-delete').click(function(event) {
                event.preventDefault();
                productid = $(this).attr('data-productid');
                swal({
                    title: "Are you sure?",
                    text: "You want to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    $('form#delete_'+productid).submit();
                    //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                });
            });
        });
    </script>
@endsection