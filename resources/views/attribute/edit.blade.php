
@extends('index')

@section('title')
	Edit Attribute
@endsection

@section('extra-css')

@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Editing Attribute
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/attributes/') }}" class="btn bg-deep-orange waves-effect" title="Back to Attribute">
                                <i class="material-icons">reply</i>
                                <span class="sr-only">Back to Attribute</span>
                            </a>
                        </li>
                    </ul> 
                </div>
                <div class="body">
                    <div class="body">
                    {!! Form::model($attribute, array('action' => array('AttributeController@update', $attribute->id), 'method' => 'PUT','id' => 'form_validation_station')) !!}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            {!! Form::text('name', NULL, array('id' => 'name', 'class' => 'form-control','required')) !!}
                                            {!! Form::label('name', 'Name' , array('class' => 'form-label')); !!}
                                        </div>
                                    </div>
                                </div>                                 
                            </div>
                            <button type="submit" class="btn btn-primary waves-effect">
                                <i class="material-icons">save</i>
                                <span>Save Changes</span>
                            </button>
                            <a href="{{ url('/attributes/') }}" class="btn bg-deep-orange waves-effect" title="Back to Attribute">
                                  <i class="material-icons">reply</i>
                                  <span class="sr-only">Back to Attribute</span>
                              </a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')    
@endsection