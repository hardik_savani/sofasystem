@extends('index')

@section('title')
    Users List
@endsection

@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css">
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">
        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Users List
                        <small style="display: inline-block;">{{ count($users) }} Total Users</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/users/create') }}" class="mdl-button mdl-button--icon bg-deep-orange waves-effect" title="Add New User">
                                <i class="material-icons">add</i>
                                <span class="sr-only">Add New User</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="" class="table table-bordered table-striped table-hover js-basic-example dataTable mdl-data-table data-table-user" style="width:100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Role</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->first_name}}</td>
                                        <td>{{$user->last_name}}</td>
                                        <td>
                                            @foreach ($user->roles as $user_role)
                                                @if ($user_role->name == 'Employee')
                                                    @php
                                                        $levelIcon        = 'Employee';
                                                        $levelName        = 'Employee';
                                                        $levelBgClass     = 'mdl-color--blue-200';
                                                        $leveIconlBgClass = 'mdl-color--blue-500';
                                                    @endphp
                                                @elseif ($user_role->name == 'Admin')
                                                    @php
                                                        $levelIcon        = 'supervisor_account';
                                                        $levelName        = 'Admin';
                                                        $levelBgClass     = 'mdl-color--red-200';
                                                        $leveIconlBgClass = 'mdl-color--red-500';
                                                    @endphp
                                                @elseif ($user_role->name == 'Supervisors')
                                                    @php
                                                        $levelIcon        = 'person_outline';
                                                        $levelName        = 'Supervisors';
                                                        $levelBgClass     = 'mdl-color--orange-200';
                                                        $leveIconlBgClass = 'mdl-color--orange-500';
                                                    @endphp
                                                @else
                                                    @php
                                                        $levelIcon        = 'person_outline';
                                                        $levelName        = 'Unverified';
                                                        $levelBgClass     = 'mdl-color--orange-200';
                                                        $leveIconlBgClass = 'mdl-color--orange-500';
                                                    @endphp
                                                @endif
                                            @endforeach
                                            {{ $user_role->name }}
                                        </td>
                                        
                                        <td>
                                            {{-- EDIT USER ICON BUTTON --}}
                                            <a href="{{ URL::to('users/' . $user->id . '/edit') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                                                <i class="material-icons mdl-color-text--orange">edit</i>
                                            </a>

                                            {{-- DELETE ICON BUTTON AND FORM CALL --}}
                                            {!! Form::open(array('url' => 'users/' . $user->id, 'class' => 'inline-block', 'id' => 'delete_'.$user->id)) !!}
                                                {!! Form::hidden('_method', 'DELETE') !!}
                                                <a href="javascript:void(0);" class="dialog-button dialiog-trigger-delete dialiog-trigger{{$user->id}} mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" data-userid="{{$user->id}}">
                                                    <i class="material-icons mdl-color-text--red">delete</i>
                                                </a>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Role</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.material.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.data-table-user').DataTable( {
                responsive: true,
                "bLengthChange": false,
                "oLanguage": {
                    "sSearch": "",
                    "sSearchPlaceholder": "Enter Keywords Here",
                },
                columnDefs: [
                {
                        targets: [ 0, 1, 2,3,4,5,6 ],
                        className: 'mdl-data-table__cell--non-numeric'
                }
                ]
            });
            $('.dialiog-trigger-delete').click(function(event) {
                event.preventDefault();
                userid = $(this).attr('data-userid');
                swal({
                    title: "Are you sure?",
                    text: "You want to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    $('form#delete_'+userid).submit();
                    //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                });
            });
        });
    </script>
@endsection