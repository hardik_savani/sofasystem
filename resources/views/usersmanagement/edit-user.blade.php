@extends('index')

@section('title')
    Editing User {{ $user->name }}
@endsection

@section('extra-css')
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Editing User: {{ $user->name }}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/users/') }}" class="btn bg-deep-orange waves-effect" title="Back to Users">
                                  <i class="material-icons">reply</i>
                                  <span class="sr-only">Back to Users</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="body">
                        {!! Form::model($user, array('action' => array('UsersManagementController@update', $user->id), 'method' => 'PUT','id' => 'form_validation_user')) !!}
                        {!! csrf_field() !!}
                        	<div class="row">
                                <div class="col-sm-6">
                                	<div class="form-group form-float">
		                                <div class="form-line">
		                                    {!! Form::text('name', old('name'), array('id' => 'name', 'class' => 'form-control', 'pattern' => '[A-Z,a-z,0-9]*','required')) !!}
		                                    {!! Form::label('name', 'Username' , array('class' => 'form-label')); !!}
		                                </div>
		                            </div>
                                </div>
                                <div class="col-sm-6">
                                	<div class="form-group form-float">
		                                <div class="form-line">
		                                    {!! Form::email('email', old('email'), array('id' => 'email', 'class' => 'form-control','required')) !!}
		                                    {!! Form::label('email', 'Email' , array('class' => 'form-label')); !!}
		                                </div>
		                            </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                	<div class="form-group form-float">
		                                <div class="form-line">
		                                    {!! Form::text('first_name', old('first_name'), array('id' => 'first_name', 'class' => 'form-control', 'pattern' => '[A-Z,a-z]*','required')) !!}
		                                    {!! Form::label('first_name', 'First Name', array('class' => 'form-label')); !!}
		                                </div>
		                            </div>
                                </div>
                                <div class="col-sm-6">
                                	<div class="form-group form-float">
		                                <div class="form-line">
		                                	{!! Form::text('last_name', old('last_name'), array('id' => 'last_name', 'class' => 'form-control', 'pattern' => '[A-Z,a-z]*','required')) !!}
		                                    {!! Form::label('last_name', 'Last Name', array('class' => 'form-label')); !!}
		                                </div>
		                            </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                	<div class="form-group form-float">
		                                <div class="form-line">
		                                    <select class="form-control show-tick" name="role" id="role" required="">\
		                                        @if ($roles->count())
		                                          @foreach($roles as $role)
		                                            <option value="{{ $role->id }}" {{ $currentRole->id == $role->id ? 'selected="selected"' : '' }}>{{ $role->name }}</option>
		                                          @endforeach
		                                        @endif
		                                    </select>
		                                </div>
		                            </div>
                                </div>
                                <div class="col-sm-6">
                                	<div class="form-group form-float">
		                                <div class="form-line1">
		                                    {{--!! Form::text('phone', old('phone'), array('id' => 'phone', 'class' => 'form-control')) !!--}}
		                                    {{--!! Form::label('phone', 'Phone', array('class' => 'form-label')); !!--}}
		                                </div>
		                            </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary waves-effect">
                                <i class="material-icons">save</i>
                                <span>Save Changes</span>
                            </button>
                            <a href="{{ url('/users/') }}" class="btn bg-deep-orange waves-effect" title="Back to Users">
                                  <i class="material-icons">reply</i>
                                  <span class="sr-only">Back to Users</span>
                              </a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
    {{Html::script('bsbmd/plugins/jquery-validation/jquery.validate.js')}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form_validation_user').validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                },
                rules: {
                    email: {
                        required: {
                            depends: function(element) {
                                return true;
                                /*if ($('#role').val() == 3){
                                   return false;
                                }else{
                                   return true;
                                }*/
                            }
                        }
                    }
                }
            });
        } );
    </script>
@endsection