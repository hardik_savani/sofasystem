@extends('index')

@section('title')
    stations List
@endsection

@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css">
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">
        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Stations List
                        <small style="display: inline-block;">{{ count($stations) }} Total Stations</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/stations/create') }}" class="mdl-button mdl-button--icon bg-deep-orange waves-effect" title="Add New Station">
                                <i class="material-icons">add</i>
                                <span class="sr-only">Add New Station</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="" class="table table-bordered table-striped table-hover js-basic-example dataTable mdl-data-table data-table-station" style="width:100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Production Line</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($stations as $station)
                                    <tr>
                                        <td><a href="{{ URL::to('stations/' . $station->id) }}">{{$station->id}}</a></td>
                                        <td><a href="{{ URL::to('stations/' . $station->id) }}">{{$station->name}}</a></td>
                                        <td>
                                            @if(isset($station->lines))
                                            <span class="badge bg-blue">{{$station->lines->linenumber}}</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{-- VIEW PRODUCT ICON BUTTON --}}
                                            <a href="{{ URL::to('stations/' . $station->id) }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="View Station Detail">
                                                <i class="material-icons mdl-color-text--blue">visibility</i>
                                            </a>

                                            {{-- EDIT PRODUCT ICON BUTTON --}}
                                            <a href="{{ URL::to('stations/' . $station->id . '/edit') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                                                <i class="material-icons mdl-color-text--orange">edit</i>
                                            </a>

                                            {{-- DELETE ICON BUTTON AND FORM CALL --}}
                                            {!! Form::open(array('url' => 'stations/' . $station->id, 'class' => 'inline-block', 'id' => 'delete_'.$station->id)) !!}
                                                {!! Form::hidden('_method', 'DELETE') !!}
                                                <a href="javascript:void(0);" class="dialog-button dialiog-trigger-delete dialiog-trigger{{$station->id}} mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" data-stationid="{{$station->id}}">
                                                    <i class="material-icons mdl-color-text--red">delete</i>
                                                </a>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Production Line</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.material.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.data-table-station').DataTable( {
                order: [[0, 'desc']],
                responsive: true,
                "bLengthChange": false,
                "oLanguage": {
                    "sSearch": "",
                    "sSearchPlaceholder": "Enter Keywords Here",
                },
                columnDefs: [
                {
                        targets: [ 0, 1, 2,3 ],
                        className: 'mdl-data-table__cell--non-numeric'
                }
                ]
            });
            $('.dialiog-trigger-delete').click(function(event) {
                event.preventDefault();
                stationid = $(this).attr('data-stationid');
                swal({
                    title: "Are you sure?",
                    text: "You want to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    $('form#delete_'+stationid).submit();
                    //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                });
            });
        });
    </script>
@endsection