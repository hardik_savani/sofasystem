@extends('index')

@section('title')
    Editing Station {{ $station->name }}
@endsection

@section('extra-css')
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
    {{Html::style('bsbmd/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Editing Station: {{ $station->name }}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/stations/') }}" class="btn bg-deep-orange waves-effect" title="Back to Station">
                                <i class="material-icons">reply</i>
                                <span class="sr-only">Back to Station</span>
                            </a>
                        </li>
                    </ul> 
                </div>
                <div class="body">
                    <div class="body">
                        {!! Form::model($station, array('action' => array('StationController@update', $station->id), 'method' => 'PUT','id' => 'form_validation_station')) !!}
                        {!! csrf_field() !!}
                        	<div class="row">
                                <div class="col-sm-6">
                                	<div class="form-group form-float">
		                                <div class="form-line">
		                                    {!! Form::text('name', old('name'), array('id' => 'name', 'class' => 'form-control', 'pattern' => '[A-Z,a-z,0-9]*','required')) !!}
		                                    {!! Form::label('name', 'Name' , array('class' => 'form-label')); !!}
		                                </div>
		                            </div>
                                </div>
                                <div class="col-sm-6">
                                	<div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="productline" id="productline" required="" title="Select Production Line">
                                                @if ($lines->count())
                                                  @foreach($lines as $line)
                                                    <option value="{{ $line->id }}" {{ ($line->id == $station->product_line_id) ? 'selected="selected"' : '' }}>{{ $line->linenumber }}</option>
                                                  @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary waves-effect">
                                <i class="material-icons">save</i>
                                <span>Save Changes</span>
                            </button>
                            <a href="{{ url('/stations/') }}" class="btn bg-deep-orange waves-effect" title="Back to Station">
                                  <i class="material-icons">reply</i>
                                  <span class="sr-only">Back to Station</span>
                              </a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
    {{Html::script('bsbmd/plugins/jquery-validation/jquery.validate.js')}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form_validation_station').validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });
        } );
    </script>
@endsection