@extends('index')

@section('title')
station {{ $station->name }}
@endsection

@section('extra-css')
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-tagsinput.css')}}
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Station: {{ $station->name }}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/stations/') }}" class="btn bg-deep-orange waves-effect" title="Back to Station">
                                <i class="material-icons">reply</i>
                                <span class="sr-only">Back to Station</span>
                            </a>
                        </li>
                    </ul>                   
                </div>
                <div class="body">
                    <div class="body">
                    	<div class="row">
                            <div class="col-sm-6">
                                <label>Name</label><br>
                            	{{$station->name}}
                            </div>
                            <div class="col-sm-6">
                                <label>Production Line</label><br>
                                <span class="badge bg-blue">{{$station->lines->linenumber}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
@endsection