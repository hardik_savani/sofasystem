@extends('index')

@section('title')
Product {{ $product->name }}
@endsection

@section('extra-css')
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-tagsinput.css')}}
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Product: {{ $product->name }}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/products/') }}" class="btn bg-deep-orange waves-effect" title="Back to Product">
                                <i class="material-icons">reply</i>
                                <span class="sr-only">Back to Product</span>
                            </a>
                        </li>
                    </ul>                   
                </div>
                <div class="body">
                    <div class="body">
                    	<div class="row">
                            <div class="col-sm-6">
                                <label>Name</label><br>
                            	{{$product->name}}
                            </div>
                            <!--div class="col-sm-6">
                                <label>Description</label><br>
                                {{$product->description}}
                            </div-->
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Production Line</label><br>
                                @foreach ($product->productlines as $key => $line)
                                    <span class="badge bg-grey">{{$line->linenumber}}</span>
                                @endforeach
                            </div>
                            <div class="col-sm-6">
                                <label>Station</label><br>
                                @foreach ($product->stations as $key => $station)
                                    <span class="badge bg-purple">{{$station->name}}</span>
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Bonus Per Minute</label><br>
                                @if(!empty($product->bonus))
                                    ${{$product->bonus}}
                                @else
                                    N/A
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label>Time</label><br>{{$product->hours}} Hours {{$product->minutes}} Minutes 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
@endsection