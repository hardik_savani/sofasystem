@extends('index')

@section('title')
    Editing Product {{ $product->name }}
@endsection

@section('extra-css')
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
    {{Html::style('bsbmd/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Editing Product: {{ $product->name }}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/products/') }}" class="btn bg-deep-orange waves-effect" title="Back to Product">
                                <i class="material-icons">reply</i>
                                <span class="sr-only">Back to Product</span>
                            </a>
                        </li>
                    </ul> 
                </div>
                <div class="body">
                    <div class="body">
                        {!! Form::model($product, array('action' => array('ProductsManagementController@update', $product->id), 'method' => 'PUT','id' => 'form_validation_product')) !!}
                        {!! csrf_field() !!}
                        	<div class="row">
                                <div class="col-sm-6">
                                	<div class="form-group form-float">
		                                <div class="form-line">
		                                    {!! Form::text('name', old('name'), array('id' => 'name', 'class' => 'form-control', 'pattern' => '[A-Z,a-z,0-9]*','required')) !!}
		                                    {!! Form::label('name', 'Name' , array('class' => 'form-label')); !!}
		                                </div>
		                            </div>
                                </div>
                                <!--div class="col-sm-6">
                                	<div class="form-group form-float">
		                                <div class="form-line">
		                                    {!! Form::textarea('description', old('description'), array('id' => 'description', 'class' => 'form-control')) !!}
		                                    {!! Form::label('description', 'Description' , array('class' => 'form-label')); !!}
		                                </div>
		                            </div>
                                </div-->
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="lines[]" id="lines" required="" title="Select Production Line">
                                                @if ($lines->count())
                                                  @foreach($lines as $line)
                                                    <option value="{{ $line->id }}" {{ in_array($line->id,$selectedLine) ? 'selected="selected"' : '' }}>{{ $line->linenumber }}</option>
                                                  @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="stations[]" id="stations" required="" data-live-search="true" title="Select Stations">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            {!! Form::input('number','bonus', old('bonus'), array('id' => 'bonus', 'class' => 'form-control')) !!}
                                            {!! Form::label('bonus', 'Bonus Per Minute' , array('class' => 'form-label')); !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            {!! Form::text('hours', old('hours'), array('id' => 'hours', 'class' => 'form-control', 'min' => 0, 'max' => '23',)) !!}
                                            {!! Form::label('hours', 'Hours' , array('class' => 'form-label')); !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            {!! Form::text('minutes', old('minutes'), array('id' => 'minutes', 'class' => 'form-control', 'min' => 0, 'max' => '59',)) !!}
                                            {!! Form::label('minutes', 'Minutes' , array('class' => 'form-label')); !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary waves-effect">
                                <i class="material-icons">save</i>
                                <span>Save Changes</span>
                            </button>
                            <a href="{{ url('/products/') }}" class="btn bg-deep-orange waves-effect" title="Back to Product">
                                  <i class="material-icons">reply</i>
                                  <span class="sr-only">Back to Product</span>
                              </a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
    {{Html::script('bsbmd/plugins/jquery-validation/jquery.validate.js')}}
    <script type="text/javascript">
        var selectedStations = {{ json_encode($selectedStations) }};
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        function getStations(id,stations = []){
            $.ajax({
                url: '/getstationbylines',
                type: 'POST',
                data: {_token: CSRF_TOKEN, id:id},
                dataType: 'JSON',
                success: function (data) {
                    if(!$.trim(data)){
                        $('#stations').find('option').remove().end();
                        $('#stations').selectpicker("refresh");
                    }else{
                        var option = '';
                        $.each(data, function(k, v) {
                            option += '<option value="'+v.id+'">'+v.name+'</option>';
                        });
                        
                        $('#stations').find('option').remove().end().append(option);
                        $('#stations').selectpicker("refresh");

                        $.each(stations, function(k, v) {
                            $('#stations').val(v);
                            $('#stations').selectpicker("refresh");
                        });
                    }
                }
            });
        }
        $(document).ready(function() {
            $('#form_validation_product').validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });
            $('#lines').on('change', function(){
                var selected = $('#lines').val();
                if(selected){
                    getStations(selected);
                }
            });
            getStations($('#lines').val(),selectedStations);
        } );
    </script>
@endsection