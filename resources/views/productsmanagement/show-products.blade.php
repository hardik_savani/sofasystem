@extends('index')

@section('title')
    products List
@endsection

@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css">
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">
        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Products List
                        <small style="display: inline-block;">{{ count($products) }} Total Products</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/products/create') }}" class="mdl-button mdl-button--icon bg-deep-orange waves-effect" title="Add New Product">
                                <i class="material-icons">add</i>
                                <span class="sr-only">Add New Product</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="" class="table table-bordered table-striped table-hover js-basic-example dataTable mdl-data-table data-table-product" style="width:100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>                                    									<th>Description</th>
                                    <th>Production Lines</th>
                                    <th>Station</th>
                                    <th>Bonus</th>
                                    <th>Duration</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td><a href="{{ URL::to('products/' . $product->id) }}">{{$product->id}}</a></td>
                                        <td><a href="{{ URL::to('products/' . $product->id) }}">{{$product->name}}</a></td>
                                        <td><a href="{{ URL::to('products/' . $product->id) }}">{{str_limit($product->description,20)}}</a></td>
                                        <td>
                                            @foreach($product->productlines as $productline)
                                                <span class="badge bg-grey">{{$productline->linenumber}}</span>
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($product->stations as $station)
                                                <span class="badge bg-purple">{{$station->name}}</span>
                                            @endforeach
                                        </td>
                                        <td>&pound;{{$product->bonus}}</td>
                                        <td>{{$product->hours}}:{{$product->minutes}}:00</td>
                                        <td>
                                            {{-- VIEW PRODUCT ICON BUTTON --}}
                                            <a href="{{ URL::to('products/' . $product->id) }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="View Product Detail">
                                                <i class="material-icons mdl-color-text--blue">visibility</i>
                                            </a>

                                            {{-- EDIT PRODUCT ICON BUTTON --}}
                                            <a href="{{ URL::to('products/' . $product->id . '/edit') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                                                <i class="material-icons mdl-color-text--orange">edit</i>
                                            </a>

                                            {{-- DELETE ICON BUTTON AND FORM CALL --}}
                                            {!! Form::open(array('url' => 'products/' . $product->id, 'class' => 'inline-block', 'id' => 'delete_'.$product->id)) !!}
                                                {!! Form::hidden('_method', 'DELETE') !!}
                                                <a href="javascript:void(0);" class="dialog-button dialiog-trigger-delete dialiog-trigger{{$product->id}} mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" data-productid="{{$product->id}}">
                                                    <i class="material-icons mdl-color-text--red">delete</i>
                                                </a>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Production Lines</th>
                                    <th>Station</th>
                                    <th>Bonus</th>
                                    <th>Duration</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.material.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.data-table-product').DataTable( {
                order: [[0, 'desc']],
                responsive: true,
                "bLengthChange": false,
                "oLanguage": {
                    "sSearch": "",
                    "sSearchPlaceholder": "Enter Keywords Here",
                },
                columnDefs: [
                {
                        targets: [ 0, 1, 2,3 ],
                        className: 'mdl-data-table__cell--non-numeric'
                }
                ]
            });
            $('.dialiog-trigger-delete').click(function(event) {
                event.preventDefault();
                productid = $(this).attr('data-productid');
                swal({
                    title: "Are you sure?",
                    text: "You want to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    $('form#delete_'+productid).submit();
                    //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                });
            });
        });
    </script>
@endsection