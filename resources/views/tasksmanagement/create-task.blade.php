
@extends('index')

@section('title')
	Create New Task
@endsection

@section('extra-css')
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
    {{Html::style('bsbmd/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}
    {{Html::style('bsbmd/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Add New Task
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/tasks/') }}" class="btn bg-deep-orange waves-effect" title="Back to Task">
                                <i class="material-icons">reply</i>
                                <span class="sr-only">Back to Task</span>
                            </a>
                        </li>
                    </ul> 
                </div>
                <div class="body">
                    <div class="body">
                        {!! Form::open(array('action' => 'TaskController@store', 'method' => 'POST', 'role' => 'form', 'id' => 'form_validation_task')) !!}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="product[]" id="product" required="" data-live-search="true" title="Select Product">
                                                @if ($products->count())
                                                  @foreach($products as $product)
                                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                  @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            {!! Form::textarea('description', NULL, array('id' => 'description', 'class' => 'form-control')) !!}
                                            {!! Form::label('description', 'Description' , array('class' => 'form-label')); !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="user" id="user" required="" data-live-search="true" title="Select User">
                                                @if ($users->count())
                                                  @foreach($users as $user)
                                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                  @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <!--<div class="form-group form-float">
                                        <div class="form-line">
                                            <select class="form-control show-tick" name="stations[]" id="stations" required="" data-live-search="true" title="Select Stations">                           
                                            </select>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        {!! Form::checkbox('bonus_applicable', '1',false,['class' => 'filled-in','id' => 'ig_checkbox']); !!}
                                        <label for="ig_checkbox">Is Bonus Applicable?</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            {!! Form::text('task_date', NULL, array('id' => 'task_date', 'class' => 'form-control','required')) !!}
                                            {!! Form::label('task_date', 'Date' , array('class' => 'form-label')); !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary waves-effect">
                                <i class="material-icons">save</i>
                                <span>Save New Task</span>
                            </button>
                            <a href="{{ url('/tasks/') }}" class="btn bg-deep-orange waves-effect" title="Back to tasks">
                                  <i class="material-icons">reply</i>
                                  <span class="sr-only">Back to Tasks</span>
                              </a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
    {{Html::script('bsbmd/plugins/jquery-validation/jquery.validate.js')}}
    {{Html::script('bsbmd/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}
    {{Html::script('bsbmd/plugins/momentjs/moment.js')}}
    {{Html::script('bsbmd/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}
    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        function getLines(id){
            $.ajax({
                url: '/getproductionlist',
                type: 'POST',
                data: {_token: CSRF_TOKEN, id:id},
                dataType: 'JSON',
                success: function (data) {
                    if(!$.trim(data)){
                        $('#productionline').find('option').remove().end();
                        $('#productionline').selectpicker("refresh");
                    }else{
                        var option = '';
                        $.each(data, function(k, v) {
                            option += '<option value="'+v.id+'">'+v.linenumber+'</option>';
                        });
                        
                        $('#productionline').find('option').remove().end().append(option);
                        $('#productionline').selectpicker("refresh");
                    }
                }
            });
        }
        function getStations(id){
            $.ajax({
                url: '/getstationlist',
                type: 'POST',
                data: {_token: CSRF_TOKEN, id:id},
                dataType: 'JSON',
                success: function (data) {
                    if(!$.trim(data)){
                        $('#stations').find('option').remove().end();
                        $('#stations').selectpicker("refresh");
                    }else{
                        var option = '';
                        $.each(data, function(k, v) {
                            option += '<option value="'+v.id+'">'+v.name+'</option>';
                        });
                        
                        $('#stations').find('option').remove().end().append(option);
                        $('#stations').selectpicker("refresh");
                    }
                }
            });
        }
        function getProduct(id){
            $.ajax({
                url: '/getproduct',
                type: 'GET',
                data: {_token: CSRF_TOKEN, id:id},
                dataType: 'JSON',
                success: function (data) {
                    $('#description').html(data.description).focus();
                }
            });
        }
        
        $(document).ready(function() {
            $('#task_date').bootstrapMaterialDatePicker({ format : 'MM/DD/YYYY',weekStart : 0, time: false,minDate : new Date() });
            $('#form_validation_task').validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });

            $('#product').on('change', function(){
                var product = $('#product').val();
                getProduct(product);
            });
        } );
    </script>
@endsection