@extends('index')

@section('title')
    Tasks List
@endsection

@section('extra-css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css">
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">
        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Tasks List
                        <small style="display: inline-block;">{{ count($tasks) }} Total Tasks</small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/reorder') }}" class="mdl-button mdl-button--icon bg-amber waves-effect" title="ReOrder Task">
                                <i class="material-icons">autorenew</i>
                                <span class="sr-only">ReOrder Task</span>
                            </a>
                            <a href="{{ url('/tasks/create') }}" class="mdl-button mdl-button--icon bg-deep-orange waves-effect" title="Add New Task">
                                <i class="material-icons">add</i>
                                <span class="sr-only">Add New Task</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table id="" class="table table-bordered table-striped table-hover js-basic-example dataTable mdl-data-table data-table-task" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Priority</th>
                                    <th>Product</th>
                                    <th>Description</th>
                                    <th>User</th>                                    
                                    <th>Station</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tasks as $task)
                                    <tr>
                                        <td><a href="{{ URL::to('tasks/' . $task->id) }}">{{$task->priority}}</a></td>
                                        <td>
                                            @if(!empty($task->products))
                                                @foreach ($task->products as $product)
                                                    <a href="{{ URL::to('products/' . $product->id) }}">
                                                    {{$product->name}}
                                                    </a>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td><a href="{{ URL::to('tasks/' . $task->id) }}">{{str_limit($task->description,20)}}</a></td>
                                        <td><a href="{{ URL::to('tasks/' . $task->id) }}">{{($task->user)?$task->user->name:''}}</a></td>
                                        <td>
                                            @if(!empty($task->products))
                                                @foreach ($task->products as $product)
                                                    @if(!empty($product->stations))
                                                        @foreach ($product->stations as $station)
                                                            <a href="{{ URL::to('stations/' . $station->id) }}">
                                                            {{$station->name}}
                                                            </a>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ URL::to('tasks/' . $task->id) }}">
                                                @if($task->status == 'Completed')
                                                    <span class="badge bg-green">
                                                @elseif($task->status == 'InProgress' || $task->status == 'Stopped')
                                                    <span class="badge bg-purple">
                                                @elseif($task->status == 'Assigned')
                                                    <span class="badge bg-orange">
                                                @endif
                                                    {{$task->status}}
                                                </span>
                                            </a></td>
                                        <td>
                                            {{-- VIEW task ICON BUTTON --}}
                                            <a href="{{ URL::to('tasks/' . $task->id) }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" title="View task Detail">
                                                <i class="material-icons mdl-color-text--blue">visibility</i>
                                            </a>

                                            {{-- EDIT task ICON BUTTON --}}
                                            <a href="{{ URL::to('tasks/' . $task->id . '/edit') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                                                <i class="material-icons mdl-color-text--orange">edit</i>
                                            </a>

                                            {{-- DELETE ICON BUTTON AND FORM CALL --}}
                                            {!! Form::open(array('url' => 'tasks/' . $task->id, 'class' => 'inline-block', 'id' => 'delete_'.$task->id)) !!}
                                                {!! Form::hidden('_method', 'DELETE') !!}
                                                <a href="javascript:void(0);" class="dialog-button dialiog-trigger-delete dialiog-trigger{{$task->id}} mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" data-taskid="{{$task->id}}">
                                                    <i class="material-icons mdl-color-text--red">delete</i>
                                                </a>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Priority</th>
                                    <th>Product</th>
                                    <th>Description</th>
                                    <th>User</th>                                    
                                    <th>Station</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.material.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.data-table-task').DataTable( {
                order: [[0, 'asc']],
                responsive: true,
                "bLengthChange": false,
                "oLanguage": {
                    "sSearch": "",
                    "sSearchPlaceholder": "Enter Keywords Here",
                },
                columnDefs: [
                {
                    targets: [ 0, 1, 2,3,4,5,6 ],
                    className: 'mdl-data-table__cell--non-numeric'
                }
                ]
            });
            $('.dialiog-trigger-delete').click(function(event) {
                event.preventDefault();
                taskid = $(this).attr('data-taskid');
                swal({
                    title: "Are you sure?",
                    text: "You want to delete this record!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    $('form#delete_'+taskid).submit();
                    //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                });
            });
        });
    </script>
@endsection