@if(count($tasks) > 0)
    <div class="table-responsive">
        <table class="table table-hover sorted_table">
            <thead class="sorted_head">
                <tr>
                    <th></th>
                    <th>Priority</th>
                    <th>Product</th>
                    <th>Station</th>
                    <th>Duration</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tasks as $k=>$task)
                    <tr data-priority="{{$task->priority}}" data-id="{{$task->id}}">
                    	<td><i class="material-icons">drag_handle</i></td>
                        <td>{{$task->priority}}</td>
                        <td>{{($task->products->first())?$task->products->first()->name:''}}</td>
                        <td>{{($task->products->first())?$task->products->first()->stations->first()->name:''}}</td>
                        <td>
                            @if($task->products->first())
                                {{$task->products->first()->days}} Days {{$task->products->first()->hours}} Hours {{$task->products->first()->minutes}} Minutes
                            @endif
                        </td>
                        <td>
                            @if($task->status == 'Completed')
                                <span class="badge bg-green">
                            @elseif($task->status == 'InProgress' || $task->status == 'Stopped')
                                <span class="badge bg-purple">
                            @elseif($task->status == 'Assigned')
                                <span class="badge bg-orange">
                            @endif
                                {{$task->status}}
                            </span>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>    
@else	    
    <h2>No Records Found!</h2>	    
@endif