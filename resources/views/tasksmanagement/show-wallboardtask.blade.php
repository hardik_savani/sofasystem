<div class="card" style="min-height: calc(100vh - 50px);">
    <div class="row clearfix">
        @if(count($tasks) > 0)    
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 div1">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">Priority</th>
                                <th class="text-center">Product</th>
                                <th class="text-center">Station</th>
                                <th class="text-center">Duration</th>
                                <th class="text-center">Exceeded Time</th>
                                <th class="text-center">Bonus</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $first = true;
                                $totalBonus = 0;
                            @endphp
                            @foreach($tasks as $k=>$task)
                                @php
                                    $bg = '';
                                    if($task->status == 'Completed'){
                                        $bg = 'bg-grey';
                                    }else{
                                        if($first){
                                            $first = false;
                                            $bg = 'bg-green';
                                        }
                                    }
                                @endphp
                                <tr class="{{$bg}}" data-id="{{$task->id}}">
                                    <td>{{$task->priority}}</td>
                                    <td>{{$task->products->first()->name}}</td>
                                    <td>{{$task->products->first()->stations->first()->name}}</td>
                                    <td>{{str_pad($task->products->first()->hours,2,"0",STR_PAD_LEFT)}}:{{str_pad($task->products->first()->minutes,2,"0",STR_PAD_LEFT)}}:00</td>
                                    <td>
                                    @php
                                        $totalDuration = $day = $hours = $minutes = 0;
                                        $sign = '';
                                        if($task->status != 'Assigned'){
                                            foreach($task->log as $k => $log){
                                                if(!empty($log->end_time)){
                                                    $totalDuration += $log->end_time->diffInSeconds($log->start_time);
                                                }else{
                                                    $totalDuration += 0;
                                                }                                            
                                            }
                                            $taskDuration = ($task->products->first()->days *24*60*60)+($task->products->first()->hours*60*60)+($task->products->first()->minutes*60);
                                            if($taskDuration<$totalDuration){//overdue
                                                $diffInSeconds = $totalDuration - $taskDuration;
                                                $sign = '';
                                            }else{
                                                $diffInSeconds = $taskDuration - $totalDuration;
                                                $sign = '-';
                                            }
                                            //$day   = floor($diffInSeconds / 86400);
                                            $day   = 0;
                                            $hours = floor(($diffInSeconds -($day*86400)) / 3600);
                                            $minutes = floor(($diffInSeconds / 60) % 60);
                                            $seconds = floor(($diffInSeconds) % 60);
                                        }
                                    @endphp
                                        {{$sign}}{{str_pad($hours,2,"0",STR_PAD_LEFT)}}:{{str_pad($minutes,2,"0",STR_PAD_LEFT)}}:{{str_pad($seconds,2,"0",STR_PAD_LEFT)}}
                                    </td>

                                    @php
                                        $userbonus = $totalDiff = $seconds = 0;
                                        if($task->status == 'Completed'){
                                            $userbonus = $task->usertask()->first()->pivot->bonus;
                                        }elseif($task->status == 'Assigned'){
                                            $userbonus = 0;
                                        }elseif($task->bonus_applicable){
                                            if(count($task->log) > 0){
                                                foreach($task->log as $log){
                                                    if(!empty($log->end_time)){
                                                        $seconds += $log->start_time->diffInSeconds($log->end_time);
                                                    }else{
                                                        $seconds += 0;
                                                    }
                                                }
                                            }
                                            $taskDuration = ($task->products->first()->days *24*60*60)+($task->products->first()->hours*60*60)+($task->products->first()->minutes*60);
                                            
                                            if($seconds > $taskDuration){
                                                $status = 'overdue';
                                                $totalDiff = $seconds - $taskDuration;
                                                if($task->bonus_applicable){
                                                    $userbonus = (-1)*($totalDiff * ($task->products->first()->bonus)/60);
                                                }
                                            }else{
                                                $status = 'beforetime';
                                                $totalDiff = $taskDuration - $seconds;
                                                if($task->bonus_applicable){
                                                    $userbonus = $totalDiff * ($task->products->first()->bonus/60);
                                                }
                                            }
                                        }
                                        $totalBonus += $userbonus;
                                    @endphp
                                    <td>&pound;{{number_format($userbonus,2)}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <thead>
                            <tr>
                                <th class="text-right" colspan="5">Total</th>
                                <th class="text-center">&pound;{{number_format($totalBonus,2)}}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 div2">
                @if(count($tasks) > 0) 
                    @foreach($tasks as $k=>$task)
                        @php
                            if($task->status == 'Completed'){
                                continue;
                            }
                            //$arrColor = ['bg-cyan','bg-green','bg-purple','bg-indigo','bg-grey','bg-teal','bg-blue-grey'];
                            $arrColor = ['bg-green','bg-amber','bg-red'];
                            $colorKey = 0;
                            $seconds = 0;
                            if($task->status == 'InProgress' || $task->status == 'Stopped' || $task->status == 'Completed'){
                                foreach($task->log as $log){
                                    $dateStart = \Carbon\Carbon::parse($log->start_time);
                                    if(!empty($log->end_time)){
                                        $dateEnd = \Carbon\Carbon::parse($log->end_time);
                                    }else{
                                        $dateEnd = \Carbon\Carbon::now();
                                    }
                                    $seconds += $dateStart->diffInSeconds($dateEnd);
                                }
                            }
                            $taskDuration = ($task->products->first()->days *24*60*60)+($task->products->first()->hours*60*60)+($task->products->first()->minutes*60);
                            $halfTime  = ($taskDuration * 1000)/2;
                        @endphp
                        <div class="container stopwatch stopwatch{{$task->id}} {{$arrColor[$colorKey]}}" data-id="{{$task->id}}" data-sec="{{$seconds}}" data-status="{{$task->status}}" data-duration={{$taskDuration}} style="min-height: calc(100vh - 50px);">
                            <br>
                            <a class="div-close waves-effect waves-teal btn-flat pull-left" data-toggle="tooltip" data-placement="bottom" title="Close" style="display: none;">
                                <i class="material-icons wallboard-icons" style="transform: scaleX(-1)">reply</i>
                            </a>
                            @if ($task->products->count())
                                @foreach($task->products as $product)
                                    <span class="badge bg-indigo badge-wallboard" data-toggle="tooltip" data-placement="bottom" title="{{$task->user->name}}">{{$task->user->name}}</span>
                                    <span class="badge bg-purple badge-wallboard" data-toggle="tooltip" data-placement="bottom" title="{{$product->name}}">{{$product->name}}</span>

                                    @if ($product->stations->count())
                                        @foreach($product->stations as $station)
                                            <span class="badge bg-teal badge-wallboard" data-toggle="tooltip" data-placement="bottom" title="{{$station->name}}">{{$station->name}}</span>
                                        @endforeach
                                    @endif
                                    <span class="badge bg-purple badge-wallboard" data-toggle="tooltip" data-placement="bottom" title="Bonus">&pound;{{number_format($totalBonus,2)}}</span>
                                    <br>
                                @endforeach
                            @endif
                            <a class="stopwatch-btn-start waves-effect waves-teal btn-flat" data-id="{{$task->id}}" data-toggle="tooltip" data-placement="bottom" title="Start">
                                <i class="material-icons wallboard-icons">play_circle_filled</i>
                            </a>
                            <a class="stopwatch-btn-pause waves-effect waves-teal btn-flat" data-id="{{$task->id}}" data-toggle="tooltip" data-placement="bottom" title="Pause">
                                <i class="material-icons wallboard-icons">pause_circle_filled</i>
                            </a>
                            <a class="stopwatch-wallboard-complete waves-effect waves-teal btn-flat" data-id="{{$task->id}}" data-toggle="tooltip" data-placement="bottom" title="Mark as Complete">
                                <i class="material-icons wallboard-icons">check_circle</i>
                            </a>                        
                            <div class="clock inactive z-depth-1 wallboardclock">
                                <span>0:00:00.0</span>
                                <div class="overlay1 waves-effect"></div>
                            </div>
                        </div>
                        <script type="text/javascript">
                            halfTime  = '{{$halfTime}}';
                        </script>
                        @break;
                    @endforeach
                @endif
            </div>        
        @else
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>No Records Found!</h2>
        </div>
        @endif
    </div>
</div>