@extends('index')

@section('title')
    Re-Order Tasks
@endsection

@section('extra-css')
    {{ Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
    {{ Html::style('bsbmd/plugins/nestable/jquery-nestable.css')}}
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">
        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Re-Order Tasks
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="{{ url('/tasks/') }}" class="btn bg-deep-orange waves-effect" title="Back to Task">
                                <i class="material-icons">reply</i>
                                <span class="sr-only">Back to Task</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    @if($role != 'employee')
                	<div class="row">
                        <div class="col-xs-6 col-sm-2">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <select class="form-control show-tick" name="user" id="user"data-live-search="true" title="Select User">
                                        @if ($users->count())
                                          @foreach($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                          @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                   	</div>
                    @else
                        <input type="hidden" name="user" id="user" value="{{auth()->user()->id}}">
                    @endif
                   	<div class="row">
                        <div class="col-xs-12 col-sm-12" id="divlist">

	                    </div>
                   	</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
	{{Html::script('bsbmd/plugins/nestable/jquery.nestable.js')}}
	{{Html::script('https://code.jquery.com/ui/1.12.1/jquery-ui.js')}}
	{{Html::script('bsbmd/plugins/bootstrap-notify/bootstrap-notify.js')}}
    {{Html::script('bsbmd/js/pages/ui/notifications.js')}}
    <script type="text/javascript">
    	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    	function getUserTask(user){
            $.ajax({
                url: '/getusertasklist',
                type: 'POST',
                data: {_token: CSRF_TOKEN, user:user},
                dataType: 'JSON',
                success: function (data) {
                	$('#divlist').html(data.content);
                	tasksortable();
                }
            });
        }

        function tasksortable(){
        	if($( "tbody" ).hasClass('ui-sortable')){
	        	$( "tbody" ).sortable("destroy");
	        }
			$( "tbody" ).sortable({
			  	update: function( event, ui ) {
			  		var list = [];
			  		$(".ui-sortable tr").each(function(i, el){
			            list.push($(el).attr('data-id'));
			        });
			        updateUserTaskOrder(list);
			  	}
			});
			$( "tbody" ).disableSelection();
        }

        function updateUserTaskOrder(tasks){
            $.ajax({
                url: '/updateusertaskorder',
                type: 'POST',
                data: {_token: CSRF_TOKEN, user:$('#user').val(),tasks:tasks},
                dataType: 'JSON',
                success: function (data) {
                	getUserTask($('#user').val());
                	showNotification('bg-green', 'Task Order Updated Successfully!', 'bottom', 'right', 'animated fadeInRight', 'animated fadeOutRight');
                }
            });
        }	
        $(document).ready(function() {
            $('#user').on('change', function(){
               getUserTask($(this).val());
            });
            if($('#user').val()){
                getUserTask($('#user').val());
            }
        });
    </script>
@endsection