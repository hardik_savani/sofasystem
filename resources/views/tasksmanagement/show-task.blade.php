@extends('index')

@section('title')
Task {{ $task->id }}
@endsection

@section('extra-css')
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
    {{Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-tagsinput.css')}}
    <style type="text/css">
        .mdl-data-table__cell--non-numeric a{
            color: inherit;
        }
        .inline-block{
            display: inline-block;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- Image Gallery -->
    <div class="block-header">        
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Task: {{ $task->id }}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            @if($task->usertask->first() !== null)
                                @if($task->usertask->first()->pivot->status == 'overdue')
                                    <span class="badge bg-red">OverDue</span>
                                @elseif($task->usertask->first()->pivot->status == 'beforetime')
                                    <span class="badge bg-indigo">On/Before Time</span>
                                @endif
                            @endif
                            <span class="badge bg-green">{{$task->status}}</span>
                            <a href="{{ url('/tasks/') }}" class="btn bg-deep-orange waves-effect" title="Back to Task">
                                <i class="material-icons">reply</i>
                                <span class="sr-only">Back to Task</span>
                            </a>
                        </li>
                    </ul>                   
                </div>
                <div class="body">
                    <div class="body">
                    	<div class="row">
                            <div class="col-sm-6">
                                <label>Product</label><br>
                                @if ($task->products->count())
                                    @foreach($task->products as $product)
                                        <span class="badge bg-cyan">{{$product->name}}</span>
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label>Description</label><br>
                                {{$task->description}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>User</label><br>
                                {{$task->user->name}}
                            </div>
                            <div class="col-sm-6">
                                <label>Time</label><br>                                
                                {{$task->products->first()->days}} Days {{$task->products->first()->hours}} Hours {{$task->products->first()->minutes}} Minutes 
                            </div>
                        </div>
                        <!--<div class="row">
                            <div class="col-sm-6">
                                <label>Station</label><br>
                                @if ($task->stations->count())
                                    @foreach($task->stations as $station)
                                        <span class="badge bg-purple">{{$station->name}}</span>
                                    @endforeach
                                @endif
                            </div>
                        </div>-->
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Priority</label><br>
                                {{$task->priority}}
                            </div>
                            <div class="col-sm-6">
                                <label>Date</label><br>
                                {{$task->task_date}}
                            </div>
                        </div>
                        @if ($task->log->count())
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="" class="table table-bordered table-striped table-hover" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Start Time</th>
                                                <th>End Time</th>
                                                <th>Duration</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $totalDuration = 0;
                                            @endphp
                                            @foreach($task->log as $k => $log)
                                                <tr>
                                                    <td>{{++$k}}</td>
                                                    <td>{{$log->start_time->format('m/d/Y H:i:s')}}</td>
                                                    <td>{{$log->end_time->format('m/d/Y H:i:s')}}</td>
                                                    <td>
                                                        @php
                                                            $duration = $log->end_time->diffInSeconds($log->start_time);
                                                            $totalDuration += $duration;

                                                            $day   = floor($duration / 86400);
                                                            $hours = floor(($duration -($day*86400)) / 3600);
                                                            $minutes = floor(($duration / 60) % 60);
                                                            $seconds = $duration % 60;
                                                        @endphp
                                                        @if($day != 0)
                                                            {{$day}} Days
                                                        @endif
                                                        @if($hours != 0)
                                                            {{$hours}} Hours
                                                        @endif
                                                        @if($minutes != 0)
                                                            {{$minutes}} Minutes
                                                        @endif
                                                        @if($seconds != 0)
                                                            {{$seconds}} Seconds
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach

                                            <tr>
                                                <td colspan="3" align="right"><b>Total Duration</b></td>
                                                <td><b>
                                                    @php
                                                        $day   = floor($totalDuration / 86400);
                                                        $hours = floor(($totalDuration -($day*86400)) / 3600);
                                                        $minutes = floor(($totalDuration / 60) % 60);
                                                        $seconds = $totalDuration % 60;
                                                    @endphp
                                                    @if($day != 0)
                                                        {{$day}} Days
                                                    @endif
                                                    @if($hours != 0)
                                                        {{$hours}} Hours
                                                    @endif
                                                    @if($minutes != 0)
                                                        {{$minutes}} Minutes
                                                    @endif
                                                    @if($seconds != 0)
                                                        {{$seconds}} Seconds
                                                    @endif
                                                    </b>
                                                </td>
                                            </tr>
                                            @if(count($task->usertask) > 0)
                                                @if($task->usertask->first()->pivot->status == 'overdue')
                                                    <tr>
                                                        <td colspan="3" align="right"><b>Time Exceeded</b></td>
                                                        <td><b>
                                                            @php
                                                                $diffInSeconds = $task->usertask->first()->pivot->user_time - $task->usertask->first()->pivot->task_time;

                                                                $day   = floor($diffInSeconds / 86400);
                                                                $hours = floor(($diffInSeconds -($day*86400)) / 3600);
                                                                $minutes = floor(($diffInSeconds / 60) % 60);
                                                                $seconds = $diffInSeconds % 60;
                                                            @endphp
                                                            @if($day != 0)
                                                                {{$day}} Days
                                                            @endif
                                                            @if($hours != 0)
                                                                {{$hours}} Hours
                                                            @endif
                                                            @if($minutes != 0)
                                                                {{$minutes}} Minutes
                                                            @endif
                                                            @if($seconds != 0)
                                                                {{$seconds}} Seconds
                                                            @endif
                                                            </b>
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td colspan="3" align="right"><b>Total Remaining Time</b></td>
                                                        <td>
                                                            <b>
                                                            @php
                                                                $diffInSeconds = $task->usertask->first()->pivot->task_time - $task->usertask->first()->pivot->user_time;

                                                                $day   = floor($diffInSeconds / 86400);
                                                                $hours = floor(($diffInSeconds -($day*86400)) / 3600);
                                                                $minutes = floor(($diffInSeconds / 60) % 60);
                                                                $seconds = $diffInSeconds % 60;
                                                            @endphp
                                                            @if($day != 0)
                                                                {{$day}} Days
                                                            @endif
                                                            @if($hours != 0)
                                                                {{$hours}} Hours
                                                            @endif
                                                            @if($minutes != 0)
                                                                {{$minutes}} Minutes
                                                            @endif
                                                            @if($seconds != 0)
                                                                {{$seconds}} Seconds
                                                            @endif
                                                            </b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" align="right"><b>Bonus</b></td>
                                                        <td>
                                                            <b>
                                                                {{$task->usertask->first()->pivot->bonus}}
                                                            </b>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra-script')
@endsection