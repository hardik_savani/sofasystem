@if (session('message'))
  <div class="alert bg-green alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      {{ session('message') }}
  </div>
@endif

@if (session('success'))
  <div class="alert bg-green alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      {{ session('success') }}
  </div>
@endif

@if (session('status'))
  @if(session()->get('status') == 'wrong')   
    <div class="alert bg-red alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {{ session('message') }}
    </div>
  @else
    <div class="alert bg-green alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {{ session('status') }}
    </div>
  @endif
@endif

@if (session('notice'))
  <div class="alert bg-orange alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {{ session('notice') }}
    </div>
@endif

@if (session('anError'))
  <div class="alert bg-red alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {{ session('anError') }}
    </div>
@endif

@if (session('error'))
   <div class="alert bg-red alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {{ session('error') }}
    </div>
@endif

@if (count($errors) > 0)
  <div class="alert bg-red alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4>
            Error
        </h4>
        <p>
          @foreach ($errors->all() as $error)
            {{ $error }} <br />
          @endforeach
        </p>
    </div>
@endif