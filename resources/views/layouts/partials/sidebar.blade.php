@php
    $role = auth()->user()->getRoles()->first()->slug;
@endphp
<section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <!--<div class="image">
                    <img src="bsbmd/images/user.png" width="48" height="48" alt="User" />
                </div>-->
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
                    <div class="email">{{ Auth::user()->email }}</div>
                    <div class="btn-group user-helper-dropdown">
                        <a href="javascript:void(0);" style="color: #fff;" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" title="Sign Out"><i class="material-icons">input</i></a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li {{Route::is('dashboard')? 'class=active':''}}>
                        <a href="{{route('dashboard')}}">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    @if($role != 'employee')
                        <li {{(Request::is('productionionlines') || Request::is('productionionlines/*'))? 'class=active':''}}>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">line_style</i>
                                <span>Production Lines</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="{{route('productionionlines')}}">
                                        <span>All Production Lines</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/productionionlines/create') }}">
                                        <span>Add Production Line</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                         <li {{(Request::is('stations') || Request::is('stations/*'))? 'class=active':''}}>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">place</i>
                                <span>Stations</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="{{route('stations')}}">
                                        <span>All Stations</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/stations/create') }}">
                                        <span>Add Station</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li {{(Request::is('products') || Request::is('products/*'))? 'class=active':''}}>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">local_parking</i>
                                <span>Products</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="{{route('products')}}">
                                        <span>All Products</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/products/create') }}">
                                        <span>Add Product</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li {{(Request::is('tasks') || Request::is('tasks/*') || Request::is('reorder'))? 'class=active':''}}>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">layers</i>
                                <span>Tasks</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="{{route('tasks')}}">
                                        <span>All Tasks</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/tasks/create') }}">
                                        <span>Add Task</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/reorder') }}">
                                        <span>Re-Order Task</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
						<li {{(Request::is('attributes') || Request::is('attributes/*'))? 'class=active':''}}>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">local_parking</i>
                                <span>attributes</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="{{route('attributes')}}">
                                        <span>All attributes</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/attributes/create') }}">
                                        <span>Add attributes</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @else
                        <!--<li {{(Request::is('tasks') || Request::is('tasks/*') || Request::is('reorder'))? 'class=active':''}}>
                            <a href="{{route('tasks')}}">
                                <i class="material-icons">layers</i>
                                <span>Tasks</span>
                            </a>
                        </li>-->
                    @endif										@if($role != 'employee')						
                    <li {{Route::is('timer')? 'class=active':''}}>
                        <a href="{{route('timer')}}" target="_blank">
                            <i class="material-icons">timer</i>
                            <span>Timer</span>
                        </a>
                    </li>					@endif
                    <li {{Route::is('wallboards')? 'class=active':''}}>
                        <a href="{{route('wallboards')}}" target="_blank">
                            <i class="material-icons">donut_small</i>
                            <span>Wallboards</span>
                        </a>
                    </li>
                    @if($role != 'employee')
                        <li {{(Request::is('users') || Request::is('users/*'))? 'class=active':''}}>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">person</i>
                                <span>Users</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="{{route('users')}}">
                                        <span>All Users</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/users/create') }}">
                                        <span>Add User</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; {{ date('Y')}} <a href="javascript:void(0);">Production Management System</a>.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <!--<li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>-->
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                    <ul class="demo-choose-skin">
                        <li data-theme="red" class="active">
                            <div class="red"></div>
                            <span>Red</span>
                        </li>
                        <li data-theme="pink">
                            <div class="pink"></div>
                            <span>Pink</span>
                        </li>
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>
                        <li data-theme="deep-purple">
                            <div class="deep-purple"></div>
                            <span>Deep Purple</span>
                        </li>
                        <li data-theme="indigo">
                            <div class="indigo"></div>
                            <span>Indigo</span>
                        </li>
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="light-blue">
                            <div class="light-blue"></div>
                            <span>Light Blue</span>
                        </li>
                        <li data-theme="cyan">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="teal">
                            <div class="teal"></div>
                            <span>Teal</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="light-green">
                            <div class="light-green"></div>
                            <span>Light Green</span>
                        </li>
                        <li data-theme="lime">
                            <div class="lime"></div>
                            <span>Lime</span>
                        </li>
                        <li data-theme="yellow">
                            <div class="yellow"></div>
                            <span>Yellow</span>
                        </li>
                        <li data-theme="amber">
                            <div class="amber"></div>
                            <span>Amber</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="deep-orange">
                            <div class="deep-orange"></div>
                            <span>Deep Orange</span>
                        </li>
                        <li data-theme="brown">
                            <div class="brown"></div>
                            <span>Brown</span>
                        </li>
                        <li data-theme="grey">
                            <div class="grey"></div>
                            <span>Grey</span>
                        </li>
                        <li data-theme="blue-grey">
                            <div class="blue-grey"></div>
                            <span>Blue Grey</span>
                        </li>
                        <li data-theme="black">
                            <div class="black"></div>
                            <span>Black</span>
                        </li>
                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="settings">
                    <div class="demo-settings">
                        <p>GENERAL SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Report Panel Usage</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Email Redirect</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>SYSTEM SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Notifications</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Auto Updates</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>ACCOUNT SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Offline</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Location Permission</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>
        <!-- #END# Right Sidebar -->
    </section>