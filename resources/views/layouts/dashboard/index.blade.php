@extends('index')

@section('title')
	Dashboard
@endsection

@section('extra-css')
    {{ Html::style('bsbmd/plugins/node-waves/waves.css') }}
    {{ Html::style('bsbmd/plugins/animate-css/animate.css') }}
    {{ Html::style('bsbmd/plugins/morrisjs/morris.css') }}
    {{ Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
@endsection

@section('content')
	<div class="container-fluid">
        <!-- Widgets -->
        <div class="row clearfix">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">line_style</i>
                    </div>
                    <div class="content">
                        <div class="text">NEW TASKS</div>
                        <div class="number count-to" data-from="0" data-to="{{$newTasks}}" data-speed="15" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">help</i>
                    </div>
                    <div class="content">
                        <div class="text">INPROGRESS TASKS</div>
                        <div class="number count-to" data-from="0" data-to="{{$inprogressTasks}}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text">COMPLETED TASKS</div>
                        <div class="number count-to" data-from="0" data-to="{{$completedTasks}}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->

        <div class="row clearfix">
            <!-- Task Info -->
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="card">
                    <div class="header">
                        <h2 style="display: inline;">TASK INFOS</h2>                        
                        <div class="form-group form-float pull-right" style="width: 100px;">
                            <div class="form-line">
                                <select class="form-control show-tick" name="task_filter" id="task_filter" title="Filter Task">
                                    <option value="1" selected="">All</option>
                                    <option value="2">New</option>
                                    <option value="3">InProgress</option>
                                    <option value="4">Completed</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="body" id="dashboard-task-infos">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User</th>
                                        <th>Completion Status</th>
                                        <th>Task Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Task Info -->
            <!-- Browser Usage -->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="card">
                    <div class="body">
                        <div id="donut_chart" class="dashboard-donut-chart"></div>
                    </div>
                </div>
            </div>
            <!-- #END# Browser Usage -->
        </div>
    </div>
@endsection

@section('extra-script')
    <script type="text/javascript">
        var newTasks        = '{{$newTasks}}';
        var inprogressTasks = '{{$inprogressTasks}}';
        var completedTasks  = '{{$completedTasks}}';
    </script>
    {{Html::script('bsbmd/plugins/jquery-countto/jquery.countTo.js')}}
    {{Html::script('bsbmd/plugins/raphael/raphael.min.js')}}
    {{Html::script('bsbmd/plugins/morrisjs/morris.js')}}
    {{Html::script('bsbmd/plugins/chartjs/Chart.bundle.js')}}
    {{Html::script('bsbmd/plugins/flot-charts/jquery.flot.js')}}
    {{Html::script('bsbmd/plugins/flot-charts/jquery.flot.resize.js')}}
    {{Html::script('bsbmd/plugins/flot-charts/jquery.flot.pie.js')}}
    {{Html::script('bsbmd/plugins/flot-charts/jquery.flot.categories.js')}}
    {{Html::script('bsbmd/plugins/flot-charts/jquery.flot.time.js')}}
    {{Html::script('bsbmd/plugins/jquery-sparkline/jquery.sparkline.js')}}
    {{Html::script('bsbmd/js/pages/index.js')}}

    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        function getdashboardtask(id){
            $.ajax({
                url: '/getdashboardtask',
                type: 'GET',
                data: {_token: CSRF_TOKEN, id:id},
                dataType: 'JSON',
                success: function (data) {
                    $('.dashboard-task-infos tbody').html(data);
                    $('#dashboard-task-infos').slimscroll({height: '350px',width: '100%'});
                }
            });
        }
        $(document).ready(function() {
            $('#task_filter').on('change', function(){
                var selected = $('#task_filter').val();
                if(selected){
                    getdashboardtask(selected);
                }
            });
            getdashboardtask($('#task_filter').val());
        } );
    </script>
@endsection