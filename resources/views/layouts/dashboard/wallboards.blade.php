@extends('indextimer')

@section('title')
	Dashboard
@endsection

@section('extra-css')
    {{ Html::style('bsbmd/plugins/node-waves/waves.css') }}
    {{ Html::style('bsbmd/plugins/animate-css/animate.css') }}
    {{ Html::style('bsbmd/plugins/morrisjs/morris.css') }}
    {{ Html::style('bsbmd/plugins/stopwatch/assets/css/styles.css')}}
    {{ Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
    
@endsection

@section('content')
@php
    $arrColor = ['bg-green','bg-amber','bg-red'];
@endphp
	<div class="container-fluid">
       <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header1">
                    	<form class="form-inline" name="form_wallboard" id="form_validation_wallboard" action="POST" style="margin: 0px auto;">
	                        <div class="row">
	                            <div class="col-xs-6 col-sm-2">
	                                <div class="form-group form-float">
	                                    <div class="form-line">
	                                        <select class="form-control show-tick" name="station" id="station" required="" data-live-search="true" title="Select Station">
	                                            @if ($stations->count())
	                                              @foreach($stations as $station)
	                                                <option value="{{ $station->id }}">{{ $station->name }}</option>
	                                              @endforeach
	                                            @endif
	                                        </select>
	                                    </div>
	                                </div>
	                            </div>
	                            @if($role != 'employee')
		                            <div class="col-xs-6 col-sm-2">
		                                <div class="form-group form-float">
		                                    <div class="form-line">
		                                        <select class="form-control show-tick" name="user" id="user" required="" data-live-search="true" title="Select User">
		                                            @if ($users->count())
		                                              @foreach($users as $user)
		                                                <option value="{{ $user->id }}">{{ $user->name }}</user>
		                                              @endforeach
		                                            @endif
		                                        </select>
		                                    </div>
		                                </div>
		                            </div>
	                            @endif
	                            <div class="col-xs-6 col-sm-2">
		                            <button type="submit" class="btn btn-danger waves-effect">
	                                    <i class="material-icons">search</i>
	                                </button>
		                        </div>
	                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="timercontent">
        </div>
    </div>
@endsection

@section('extra-script')
	{{Html::script('bsbmd/plugins/jquery-validation/jquery.validate.js')}}
    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        var interval = null;
        taskId = '';
        var chkuser = '';

        var fullwidth = false;

        @if($role != 'employee')
            chkuser = true;
        @else
            chkuser = false;
        @endif

        function getTask(station, user = ''){
        	$('#task').find('option').remove().end();
            $('#task').selectpicker("refresh");
            $.ajax({
                url: '/gettasklist',
                type: 'POST',
                data: {_token: CSRF_TOKEN, station:station, user:user},
                dataType: 'JSON',
                success: function (data) {
                    if(!$.trim(data)){
                        $('#task').find('option').remove().end();
                        $('#task').selectpicker("refresh");
                    }else{
                        var option = '';
                        $.each(data, function(k, v) {
                            option += '<option value="'+v.id+'">'+v.name+'</option>';
                        });
                        
                        $('#task').find('option').remove().end().append(option);
                        $('#task').selectpicker("refresh");
                    }
                }
            });
        }

        function getwallboardtask(){
            clearInterval(interval);
            if(taskId){
                var skip = true;
                var userId = '';
                if(chkuser == true){
                    if($('#user').val() != ''){
                        skip = false;
                        userId = $('#user').val();
                    }
                }else{
                    skip = false;
                }
                if(!skip){
                	$.ajax({
                        url: '/getwallboardtask',
                        type: 'POST',
                        data: {_token: CSRF_TOKEN, station:$('#station').val(),userId:userId},
                        dataType: 'JSON',
                        success: function (r) {
                        	$('#timercontent').html(r.data);
                            if(fullwidth == true){
                                $('.stopwatch').trigger('click');
                            }
                            clearInterval(localStorage.getItem('stopwatchInterval'+$('.stopwatch').attr('data-id')));
                        	initTimer();
                            interval = setInterval(getwallboardtask,2000);
                            /*if(r.status == 'Completed'){
                                clearInterval(interval);
                            }*/
                        },
                        error: function(){
                            clearInterval(interval);
                        }
                    });
                }else{
                    taskId = '';
                }
            }
        }
        $(document).ready(function() {
            $('#form_validation_wallboard').validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                },
                submitHandler: function (form) {
                	getwallboardtask();
                    //taskId = $('#task').val();
                    taskId = 1;
                    fullwidth = false;
                	return false;
              	}
            });

            $('#user').on('change', function(){
                //getwallboardtask();
            });
            
            $('#station').on('change', function(){
                //getwallboardtask()
            });
            $('body').on('click', '.stopwatch',function(e){
                var ele_parent = $(e.target).parent()
                if(ele_parent.hasClass('stopwatch-btn-start') || ele_parent.hasClass('stopwatch-btn-pause') ||ele_parent.hasClass('stopwatch-wallboard-complete')){
                    e.preventDefault();
                    return;
                }
                if(ele_parent.hasClass('div-close')){
                    $('.div1').show();
                    $('.div2').removeClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 fullwidth').addClass('col-lg-6 col-md-6 col-sm-6 col-xs-12');
                    $('.div-close').hide();
                    fullwidth = false;
                    return
                }
                fullwidth = true;
                $('.div1').hide();
                $('.div2').removeClass('col-lg-6 col-md-6 col-sm-6 col-xs-12').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 fullwidth');
                $('.div-close').show();
            });
        } );
    </script>
    {{Html::script('bsbmd/plugins/stopwatch/assets/js/stopwatch.js')}}
    {{Html::script('bsbmd/plugins/stopwatch/assets/jonthornton-jquery-timepicker/jquery.timepicker.min.js')}}
    {{Html::script('bsbmd/plugins/stopwatch/assets/js/alarm.js')}}
    {{Html::script('bsbmd/js/pages/ui/tooltips-popovers.js')}}
    {{Html::script('bsbmd/plugins/bootstrap-notify/bootstrap-notify.js')}}
    {{Html::script('bsbmd/js/pages/ui/notifications.js')}}
@endsection