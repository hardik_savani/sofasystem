@extends('indextimer')

@section('title')
	Dashboard
@endsection

@section('extra-css')
    {{ Html::style('bsbmd/plugins/node-waves/waves.css') }}
    {{ Html::style('bsbmd/plugins/animate-css/animate.css') }}
    {{ Html::style('bsbmd/plugins/morrisjs/morris.css') }}
    {{ Html::style('bsbmd/plugins/stopwatch/assets/css/styles.css')}}
    {{ Html::style('bsbmd/plugins/bootstrap-select/css/bootstrap-select.css')}}
    
@endsection

@section('content')
@php
    //$arrColor = ['bg-cyan','bg-green','bg-purple','bg-indigo','bg-grey','bg-teal','bg-blue-grey'];
    $arrColor = ['bg-green','bg-amber','bg-red'];
@endphp
	<div class="container-fluid">
        @foreach($lines as $line)
            <div class="row clearfix ng-scope">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{$line->linenumber}}
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix" style="margin-bottom: 10px;">
                                @php
                                    $colorKey = 0;
                                @endphp
                                @foreach($line->station as $station)
                                    {{--$station->name--}}
                                    @if(count($station->products) > 0)
                                        @foreach($station->products as $key => $product)
                                            @foreach($product->timertasks as $key => $task)
                                                @php
                                                    if($colorKey > 0){
                                                        $colorKey = 0;
                                                    }
                                                    $seconds = 0;
                                                    if($task->status == 'InProgress' || $task->status == 'Stopped'){
                                                        foreach($task->log as $log){
                                                            $dateStart = \Carbon\Carbon::parse($log->start_time);
                                                            if(!empty($log->end_time)){
                                                                $dateEnd = \Carbon\Carbon::parse($log->end_time);
                                                            }else{
                                                                $dateEnd = \Carbon\Carbon::now();
                                                            }
                                                            $seconds += $dateStart->diffInSeconds($dateEnd);
                                                        }
                                                    }
                                                    $taskDuration = ($task->products->first()->days *24*60*60)+($task->products->first()->hours*60*60)+($task->products->first()->minutes*60);
                                                @endphp
                                                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                                    <div class="container stopwatch stopwatch{{$task->id}} {{$arrColor[$colorKey]}}" data-id="{{$task->id}}" data-sec="{{$seconds}}" data-status="{{$task->status}}" data-duration={{$taskDuration}}>
                                                        <span class="badge bg-purple" data-toggle="tooltip" data-placement="bottom" title="{{$task->user->name}}" style="margin: 5px auto;">{{$task->user->name}}</span>
                                                        <br>
                                                        @if ($task->products->count())
                                                            @foreach($task->products as $product)
                                                                <span class="badge bg-indigo" data-toggle="tooltip" data-placement="bottom" title="{{$product->name}}">{{$product->name}}</span>
                                                            @endforeach
                                                        @endif
                                                        <span class="badge bg-teal" data-toggle="tooltip" data-placement="bottom" title="{{$station->name}}">{{$station->name}}</span>
                                                        <br>
                                                        @if($role != 'employee')
                                                            <a class="stopwatch-btn-start waves-effect waves-teal btn-flat" data-id="{{$task->id}}" data-toggle="tooltip" data-placement="bottom" title="Start">
                                                                <i class="material-icons">play_circle_filled</i>
                                                            </a>
                                                            <a class="stopwatch-btn-pause waves-effect waves-teal btn-flat" data-id="{{$task->id}}" data-toggle="tooltip" data-placement="bottom" title="Pause">
                                                                <i class="material-icons">pause_circle_filled</i>
                                                            </a>
                                                            <a class="stopwatch-btn-complete waves-effect waves-teal btn-flat" data-id="{{$task->id}}" data-toggle="tooltip" data-placement="bottom" title="Mark as Complete">
                                                                <i class="material-icons">check_circle</i>
                                                            </a>
                                                        @endif
                                                        <div class="clock inactive z-depth-1">
                                                            <span>0:00:00.0</span>
                                                            <div class="overlay1 waves-effect"></div>
                                                        </div>
                                                    </div>
                                                    @php
                                                        $colorKey++;
                                                    @endphp
                                                </div>
                                            @endforeach
                                        @endforeach
                                    @endif                                
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row clearfix" style="margin-bottom: 10px;">
            @if(count($inprogressTasks) == 0)
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h2>
                        <small>Cheers! All Task Completed !</small>
                    </h2>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('extra-script')
    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        setTimeout(function(){
           window.location.reload(1);
        }, 30000);
    </script>
    {{Html::script('bsbmd/plugins/stopwatch/assets/js/stopwatch.js')}}
    {{Html::script('bsbmd/plugins/stopwatch/assets/jonthornton-jquery-timepicker/jquery.timepicker.min.js')}}
    {{Html::script('bsbmd/plugins/stopwatch/assets/js/alarm.js')}}
    {{Html::script('bsbmd/js/pages/ui/tooltips-popovers.js')}}
    {{Html::script('bsbmd/plugins/bootstrap-notify/bootstrap-notify.js')}}
    {{Html::script('bsbmd/js/pages/ui/notifications.js')}}
@endsection