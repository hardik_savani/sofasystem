@extends('auth.app')

@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif
<form id="sign_in" class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
    {{ csrf_field() }}
	<div class="msg">Reset your password</div>
    <input type="hidden" name="token" value="{{ $token }}">
	
	<div class="input-group">
        <span class="input-group-addon">
        <i class="material-icons">person</i>
        </span>
        <div class="form-line {{ $errors->has('email') ? ' error' : '' }}">
            <input type="text" class="form-control" name="email" value="{{$email or old('email')}}" placeholder="Email" required autofocus>
        </div>
        @if ($errors->has('email'))
        <label id="name-error" class="error" for="email">{{ $errors->first('email') }}</label>
        @endif
    </div>
	<div class="input-group">
        <span class="input-group-addon">
        <i class="material-icons">lock</i>
        </span>
        <div class="form-line {{ $errors->has('password') ? ' error' : '' }}">
            <input type="password" class="form-control" name="password" placeholder="Password" required>
        </div>
        @if ($errors->has('password'))
        <label id="name-error" class="error" for="name">{{ $errors->first('password') }}</label>
        @endif
    </div>
	
	<div class="input-group">
        <span class="input-group-addon">
        <i class="material-icons">lock</i>
        </span>
        <div class="form-line {{ $errors->has('password_confirmation') ? ' error' : '' }}">
            <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
        </div>
        @if ($errors->has('password_confirmation'))
        <label id="name-error" class="error" for="name">{{ $errors->first('password_confirmation') }}</label>
        @endif
    </div>
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-block bg-pink waves-effect">
            Reset Password
            </button>
        </div>
    </div>
</form>
@endsection
