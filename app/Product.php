<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;

	/**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'bonus',
        'days',
        'hours',
        'minutes'
    ];

    public function colours()
    {
        return $this->belongsToMany('App\Colour','product_color','product_id', 'color_id');
    }

    public function types()
    {
        return $this->belongsToMany('App\Type');
    }

    public function productlines()
    {
        return $this->belongsToMany('App\ProductLine','product_product_lines','product_id','product_line_id');
    }

    public function tasks()
    {
        return $this->belongsToMany('App\Task','task_products','product_id', 'task_id');
    }

    public function stations()
    {
        return $this->belongsToMany('App\Station','products_stations','product_id', 'station_id');
    }

    public function timertasks()
    {
        $role = auth()->user()->getRoles()->first()->slug;
        if($role == 'employee'){
            return $this->belongsToMany('App\Task','task_products','product_id','task_id')->whereIn('status',['Assigned','InProgress','Stopped'])->where('user_id',auth()->user()->id);
        }else {
            return $this->belongsToMany('App\Task','task_products','product_id','task_id')->whereIn('status',['Assigned','InProgress','Stopped']);
        }        
    }
}
