<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTask extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'task_user_task';
}
