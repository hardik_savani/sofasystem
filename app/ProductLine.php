<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductLine extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_lines';
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'linenumber',
        'no_station'
    ];


    public function Products()
    {
        return $this->belongsToMany('App\Product','product_product_lines','product_line_id','product_id');
    }

    public function station()
    {
        return $this->hasMany('App\Station');
    }
}
