<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Station extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stations';
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'product_line_id'
    ];

    public function lines()
    {
        return $this->belongsTo('App\ProductLine','product_line_id');
    }

    public function tasks()
    {
        return $this->belongsToMany('App\Task','task_stations','station_id','task_id');
    }

    public function timertasks()
    {
        $role = auth()->user()->getRoles()->first()->slug;
        if($role == 'employee'){
            return $this->belongsToMany('App\Task','task_stations','station_id','task_id')->whereIn('status',['Assigned','InProgress','Stopped'])->where('user_id',auth()->user()->id);
        }else {
            return $this->belongsToMany('App\Task','task_stations','station_id','task_id')->whereIn('status',['Assigned','InProgress','Stopped']);
        }        
    }
    public function products()
    {
        return $this->belongsToMany('App\Product','products_stations','station_id','product_id');
    }
}
