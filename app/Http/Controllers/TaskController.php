<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Task;
use App\Product;
use App\ProductLine;
use App\TaskLog;
use App\User;
use App\Station;
use Validator;
use Auth;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = auth()->user()->getRoles()->first()->slug;
        if($role != 'employee'){
            $tasks = Task::with('products','productionlines','log','user')->orderBy('priority', 'asc')->get();
            $template = 'show-tasks';
        }else{
            $tasks = Task::with('products','productionlines','log','user')->orderBy('priority', 'asc')->where('user_id',auth()->user()->id)->get();
            $template = 'show-tasks-user';
        }
        return View('tasksmanagement.'.$template, compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::with('roles')->whereHas('roles', function($query) {
                        $query->where('roles.id',3);
                    })->get();
        $products = Product::all();
        $productionline = ProductLine::all();

        $data = [
            'products'       => $products,
            'productionline' => $productionline,
            'users'          => $users,
        ];

        return view('tasksmanagement.create-task')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'description'           => '',
                'user'                  => 'required',
                'product.*'             => 'required',
                'task_date'             => 'required',
            ]
        );

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        } else {
            $task = Task::create([
                    'description'      => $request->input('description'),
                    'user_id'          => $request->input('user'),
                    'bonus_applicable' => ($request->input('bonus_applicable') == null)?0:1,
                    'priority'         => 9999,
                    'task_date'        => \Carbon\Carbon::parse($request->input('task_date'))->format('Y-m-d'),
                    'status'           => 'Assigned',
                ]);
            $task->products()->attach($request->input('product'));
            //$task->stations()->attach($request->input('stations'));

            return redirect('tasks')->with('success', 'Successfully created Task!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::with('products','productionlines','log','user')->where('id',$id)->first();
        $data = [
            'task'        => $task
        ];
        return view('tasksmanagement.show-task')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::with('roles')->whereHas('roles', function($query) {
                        $query->where('roles.id',3);
                    })->get();
        $products       = Product::all();
        $productionline = ProductLine::all();

        $selectedProducts = [];
        $tasks = Task::with('products','log','user')->where('id',$id)->first();
        foreach ($tasks->products as $key => $product) {
            $selectedProducts[] = $product->id;
        }

        $data = [
            'products'       => $products,
            'productionline' => $productionline,
            'task'           => $tasks,
            'users'          => $users,
            'selectedProducts'=> $selectedProducts,
        ];
        return view('tasksmanagement.edit-task')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);

        $validator = Validator::make($request->all(),
            [
                'description'           => '',
                'user'                  => 'required',
                'product.*'             => 'required',
                'task_date'             => 'required',
            ]
        );
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        } else {
            $task->description  = $request->input('description');
            $task->user_id      = $request->input('user');
            $task->task_date    = \Carbon\Carbon::parse($request->input('task_date'))->format('Y-m-d');
            $task->bonus_applicable = ($request->input('bonus_applicable') == null)?0:1;
            $task->save();

            $task->products()->detach();
            //$task->stations()->detach();
            $task->products()->attach($request->input('product'));
            //$task->stations()->attach($request->input('stations'));
            return redirect('tasks')->with('success', 'Successfully updated Task!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
        return redirect('tasks')->with('success', 'Successfully deleted Task!');
    }

    public function getproductionlist(Request $request)
    {
        $productId = $request->id;
        $productionlines = ProductLine::with('products')->whereHas('products', function($query) use ($productId) {
                        $query->where('products.id',$productId);
                    })->get();
        $data = [];
        foreach ($productionlines as $key => $productionline) {
            $data[] = ['id' => $productionline->id,'linenumber' => $productionline->linenumber];
        }
        return response()->json($data); 
    }

    public function getdashboardtask(Request $request)
    {
        $status = [];
        if($request->id == 1){
            $status = ['Assigned','InProgress','Stopped','Completed'];
        }elseif($request->id == 2){
            $status = ['Assigned'];
        }elseif($request->id == 3){
            $status = ['InProgress','Stopped'];
        }elseif($request->id == 4){
            $status = ['Completed'];
        }
        $role = auth()->user()->getRoles()->first()->slug;
        if($role == 'employee'){
            $tasks = Task::whereIn('status',$status)->orderBy('created_at', 'desc')->where('user_id',auth()->user()->id)->get();
        }else{
            $tasks = Task::whereIn('status',$status)->orderBy('created_at', 'desc')->get();
        }

        $data = '';
        foreach ($tasks as $k =>$task) {
            $data .= "<tr>";
            $data .= "<td>".++$k."</td>";
            //$data .= "<td>".$task->name."</td>";
            $data .= "<td>".$task->user->name."</td>";
            $data .= "<td>";
            if($task->usertask->first() !== null){
                if($task->usertask->first()->pivot->status == 'overdue'){
                    $data .= '<span class="badge bg-red">OverDue</span>';
                }
                elseif($task->usertask->first()->pivot->status == 'beforetime'){
                    $data .= '<span class="badge bg-indigo">On/Before Time</span>';
                }
            }else{
                $data .= '<span class="badge bg-grey">N/A</span>';
            }
            $data .= "</td>";
            $data .= "<td>";
            if($task->status == 'Assigned'){
                $data .= "<span class='label bg-orange'>Assigned</span>";
            }elseif($task->status == 'InProgress' || $task->status == 'Stopped'){
                $data .= "<span class='label bg-blue'>InProgress</span>";
            }elseif($task->status == 'Completed'){
                $data .= "<span class='label bg-green'>Completed</span>";
            }
            $data .= "</td>";
            $data .= "</tr>";
        }
        return response()->json($data); 
    }

    public function getstationlist(Request $request)
    {
        $productId  = $request->id;
        $product    = Product::with('productlines')->where('id',$productId)->first();
        $linesId      = $product->productlines->pluck('id')->toarray();

        $stations = Station::whereIn('product_line_id',$linesId)->get();
        $data = [];
        foreach ($stations as $key => $station) {
            $data[] = ['id' => $station->id,'name' => $station->name];
        }
        return response()->json($data); 
    }
    public function getstationbylines(Request $request)
    {
        $lineId  = $request->id;
        $product = ProductLine::with('station')->where('id',$lineId)->first();
        $data = [];
        foreach ($product->station as $key => $station) {
            $data[] = ['id' => $station->id,'name' => $station->name];
        }
        return response()->json($data); 
    }

    public function gettasklist(Request $request)
    {
        $station = $request->station;
        $user    = $request->user;
        if(empty($user)){
            $user = auth()->user()->id;
        }

        $tasks = Task::with('products')->whereHas('stations', function($q) use ($station){
                        $q->where('stations.id', '=', $station);
                    })
                    ->where('user_id',$user)->get();
        $data = [];
        foreach ($tasks as $key => $task) {
            $data[] = ['id' => $task->id,'name' => $task->id .'('.$task->products->first()->name.')'];
        }
        return response()->json($data); 
    }
    public function getwallboardtask(Request $request)
    {
        $role = auth()->user()->getRoles()->first()->slug;
        $station = $request->station;
        $user    = $request->userId;
        if($user == ''){
            $user = auth()->user()->id;
        }
        $tasks = Task::with('products','productionlines','log','user')
                ->whereHas('products.stations', function($q1) use ($station){
                   $q1->where('station_id', $station); 
               })
            ->where('user_id',$user)->orderBy('priority', 'asc')->get();

        if($tasks->first()){
            $status = $tasks->first()->status;
        }else{
            $status = '';
        }
        $data = [
            'tasks'        => $tasks,
            'role'         => $role
        ];
        $content =  view('tasksmanagement.show-wallboardtask')->with($data)->render();
        return response()->json(['data' => $content,'status'=> $status]);
    }

    public function getproduct(Request $request)
    {
        $product = Product::where('id',$request->id)->first();
        return response()->json(['name'=>$product->name,'description' => $product->description]);
    }

    public function reorder()
    {
        $users = User::with('roles')->whereHas('roles', function($query) {
                        $query->where('roles.id',3);
                    })->get();
        $role = auth()->user()->getRoles()->first()->slug;
        return View('tasksmanagement.reorder', compact('users','role'));
    }

    public function getusertasklist(Request $request)
    {
        $tasks = Task::with('products','productionlines','log','user')->orderBy('priority', 'asc')->where('user_id',$request->user)->get();
        $data = [
            'tasks'        => $tasks,
        ];
        $content =  view('tasksmanagement.get-usertasklist')->with($data)->render();
        return response()->json(['content' => $content]);
    }

    public function updateusertaskorder(Request $request)
    {
        if(isset($request->user) && !empty($request->tasks)){
            foreach ($request->tasks as $key => $val) {
                $task = Task::find($val);
                $task->priority     = $key+1;
                $task->save();
            }
        }
        return response()->json('true');
    }
}