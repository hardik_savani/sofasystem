<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Colour;
use App\Type;
use App\ProductLine;
use Validator;
use Auth;

class ProductsManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->get();
        return View('productsmanagement.show-products', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lines = ProductLine::all();

        $data = [
            'lines' => $lines,
        ];

        return view('productsmanagement.create-product')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name'                  => 'required|max:255|unique:products',
                'bonus'                 => 'numeric|between:0,1000',
                'lines.*'               => 'required',
                'stations.*'            => 'required',
            ]
        );

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        } else {
            $product = Product::create([
                    'name'             => $request->input('name'),
                    'bonus'            => $request->input('bonus'),
                    'days'             => 0,
                    'hours'            => $request->input('hours'),
                    'minutes'          => $request->input('minutes')
                ]);
            //$product->colours()->attach($request->input('colour'));
            //$product->types()->attach($request->input('type'));
            $product->productlines()->attach($request->input('lines'));
            $product->stations()->attach($request->input('stations'));

            return redirect('products')->with('success', 'Successfully created Product!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::with('colours','types')->where('id',$id)->first();

        $data = [
            'product'          => $product,
        ];

        return view('productsmanagement.show-product')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lines = ProductLine::all();

        $selectedColor = $selectedStations = [];
        $product = Product::with('stations','productlines')->where('id',$id)->first();

        foreach ($product->productlines as $key => $line) {
            $selectedLine[] = $line->id;
        }

        foreach ($product->stations as $key => $station) {
            $selectedStations[] = $station->id;
        }

        $data = [
            'product'        => $product,
            'lines'          => $lines,
            'selectedLine'   => $selectedLine,
            'selectedStations'=> $selectedStations,
        ];

        return view('productsmanagement.edit-product')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        $validator = Validator::make($request->all(),
            [
                'name'                  => "required|max:255|unique:products,name,$id,id",
                'bonus'                 => 'numeric|between:0,1000',
                'lines.*'               => 'required',
                'stations.*'            => 'required',
            ]
        );
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        } else {
            $product->name         = $request->input('name');
            $product->bonus        = $request->input('bonus');
            $product->days         = 0;
            $product->hours        = $request->input('hours');
            $product->minutes      = $request->input('minutes');
            $product->save();

            //$product->colours()->detach();
            //$product->types()->detach();
            $product->productlines()->detach();
            $product->stations()->detach();
            //$product->colours()->attach($request->input('colour'));
            //$product->types()->attach($request->input('type'));
            $product->productlines()->attach($request->input('lines'));
            $product->stations()->attach($request->input('stations'));
            return redirect('products')->with('success', 'Successfully updated Product!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect('products')->with('success', 'Successfully deleted Product!');
    }
}
