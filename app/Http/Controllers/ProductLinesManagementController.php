<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductLine;
use Validator;
use Auth;

class ProductLinesManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productionionlines = ProductLine::orderBy('created_at', 'desc')->get();
        return View('productlinesmanagement.show-productlines', compact('productionionlines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();

        $data = [
            'products' => $products,
        ];

        return view('productlinesmanagement.create-productline')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'linenumber'            => 'required|max:255|unique:product_lines'
            ]
        );

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        } else {
            $productline = ProductLine::create([
                    'linenumber'      => $request->input('linenumber'),
                    'no_station'      => 1
                ]);

            return redirect('productionionlines')->with('success', 'Successfully created Production Line!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $productionline = ProductLine::with('products')->where('id',$id)->first();

        $data = [
            'productionline' => $productionline
        ];

        return view('productlinesmanagement.show-productline')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::all();

        $data = [
            'products' => $products,
        ];

        $seectedProduct = [];

        $productionline = ProductLine::with('products')->where('id',$id)->first();
        foreach ($productionline->products as $key => $product) {
            $seectedProduct[] = $product->id;
        }

        $data = [
            'productionline' => $productionline,
            'products'       => $products,
            'seectedProduct' => $seectedProduct,
        ];

        return view('productlinesmanagement.edit-productline')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productline = ProductLine::find($id);

        $validator = Validator::make($request->all(),
            [
                'linenumber'            => "required|max:255|unique:product_lines,linenumber,$id,id",
            ]
        );
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        } else {
            $productline->linenumber    = $request->input('linenumber');
            $productline->no_station  	= 1;
            $productline->save();
            //$productline->products()->detach();
            //$productline->products()->attach($request->input('products'));
            return redirect('productionionlines')->with('success', 'Successfully updated Production Line!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productionline = ProductLine::findOrFail($id);
        $productionline->delete();
        return redirect('productionionlines')->with('success', 'Successfully deleted Production Line!');
    }
}
