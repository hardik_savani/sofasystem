<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Station;
use App\ProductLine;
use Validator;
use Auth;

class StationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stations = Station::orderBy('created_at', 'desc')->get();
        return View('stationsmanagement.show-stations', compact('stations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lines = ProductLine::all();

        $data = [
            'lines' => $lines,
        ];

        return view('stationsmanagement.create-station')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'name'                  => 'required|max:255|unique:stations',
                'description'           => '',
                'productline'           => 'required',
            ]
        );

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        } else {
            $station = Station::create([
                    'name'             => $request->input('name'),
                    'description'      => $request->input('description'),
                    'product_line_id'  => $request->input('productline')
                ]);

            return redirect('stations')->with('success', 'Successfully created station!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $station = Station::where('id',$id)->first();

        $data = [
            'station'        => $station,
        ];

        return view('stationsmanagement.show-station')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lines = ProductLine::all();

        $station = Station::where('id',$id)->first();

        $data = [
            'station'        => $station,
            'lines'          => $lines
        ];

        return view('stationsmanagement.edit-station')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $station = Station::find($id);

        $validator = Validator::make($request->all(),
            [
                'name'                  => "required|max:255|unique:stations,name,$id,id",
                'description'           => '',
                'productline'           => 'required',
            ]
        );
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        } else {
            $station->name         = $request->input('name');
            $station->description  = $request->input('description');
            $station->product_line_id = $request->input('productline');
            $station->save();
            return redirect('stations')->with('success', 'Successfully updated station!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $station = Station::findOrFail($id);
        $station->delete();
        return redirect('stations')->with('success', 'Successfully deleted station!');
    }
}
