<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Validator;
use Auth;


class UsersManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $roles = Role::all();

        return View('usersmanagement.show-users', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        $data = [
            'roles' => $roles,
        ];

        return view('usersmanagement.create-user')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*if($request->input('role') == 3){
            $validator = Validator::make($request->all(),
                [
                    'name'                  => 'required|max:255|unique:users',
                    'first_name'            => 'required',
                    'last_name'             => 'required',
                    'role'                  => 'required',
                ]
            );
        }else{*/
            $validator = Validator::make($request->all(),
                [
                    'name'                  => 'required|max:255|unique:users',
                    'first_name'            => 'required',
                    'last_name'             => 'required',
                    'email'                 => 'required|email|max:255|unique:users',
                    'password'              => 'required|min:6|max:20|confirmed',
                    'password_confirmation' => 'required|same:password',
                    'role'                  => 'required',
                ]
            );
        //}

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        } else {
            $user = User::create([
                    'name'             => $request->input('name'),
                    'first_name'       => $request->input('first_name'),
                    'last_name'        => $request->input('last_name'),
                    'email'            => $request->input('email'),
                    'phone'            => $request->input('phone'),
                    'password'         => bcrypt($request->input('password')),
                    'activated'        => 1,
                ]);
            $user->attachRole($request->input('role'));

            return redirect('users')->with('success', 'Successfully created user!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();

        foreach ($user->roles as $user_role) {
            $currentRole = $user_role;
        }

        $data = [
            'user'        => $user,
            'roles'       => $roles,
            'currentRole' => $currentRole,
        ];

        return view('usersmanagement.edit-user')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentUser = Auth::user();
        $user = User::find($id);
        /*if($request->input('role') == 3){
            $validator = Validator::make($request->all(),
                [
                    'name'       => "required|max:255|unique:users,name,$id,id",
                    'first_name' => 'required',
                    'last_name'  => 'required',
                    'role'       => 'required',
                ]
            );
        }else{*/
            $validator = Validator::make($request->all(), [
                'name'      => "required|max:255|unique:users,name,$id,id",
                'email'     => "required|max:255|unique:users,email,$id,id",
                'first_name'=> 'required',
                'last_name' => 'required',
                'role'      => 'required',
            ]);
        //}
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        } else {
            $user->name         = $request->input('name');
            $user->first_name   = $request->input('first_name');
            $user->last_name    = $request->input('last_name');
            $user->phone        = $request->input('phone');
            $user->email = $request->input('email');
            $user->save();
            $user->detachAllRoles();
            $user->attachRole($request->input('role'));
            return redirect('users')->with('success', 'Successfully updated user!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('users')->with('success', 'Successfully deleted user!');
    }
}
