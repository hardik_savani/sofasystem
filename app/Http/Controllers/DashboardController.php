<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Product;
use App\ProductLine;
use App\Station;
use App\TaskLog;
use App\User;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = auth()->user()->getRoles()->first()->slug;
        if($role == 'employee'){
            $newTasks = Task::where('status','Assigned')->where('user_id',auth()->user()->id)->get()->count();
            $inprogressTasks = Task::whereIn('status',['InProgress','Stopped'])->where('user_id',auth()->user()->id)->get()->count();
            $completedTasks = Task::where('status','Completed')->where('user_id',auth()->user()->id)->get()->count();
        }else{
            $newTasks = Task::where('status','Assigned')->get()->count();
            $inprogressTasks = Task::whereIn('status',['InProgress','Stopped'])->get()->count();
            $completedTasks = Task::where('status','Completed')->get()->count();
        }
        $totalUser = $users = User::with('roles')->whereHas('roles', function($query) {
                        $query->where('roles.id',3);
                    })->get()->count();
        $data = [
            'newTasks'        => $newTasks,
            'inprogressTasks' => $inprogressTasks,
            'completedTasks'  => $completedTasks,
            'totalUser'       => $totalUser,
        ];
        return View('layouts.dashboard.index')->with($data);
    }
    /**
     * Show the Timer.
     *
     * @return \Illuminate\Http\Response
     */
    public function timer()
    {
        $role = auth()->user()->getRoles()->first()->slug;
        if($role == 'employee'){
            $inprogressTasks = Task::with('log','products')->where('user_id',auth()->user()->id)->whereIn('status',['Assigned','InProgress','Stopped'])->get();
        }else{
            $inprogressTasks = Task::with('log','products')->whereIn('status',['Assigned','InProgress','Stopped'])->get();
        }
        $products = [];
        foreach ($inprogressTasks as $key => $task) {
            $products = array_unique(array_merge($products,$task->products->pluck('id')->toarray()));
        }

        $lines = ProductLine::with('products')
                   ->whereHas('products', function($q) use ($products){
                       $q->whereIn('product_id', $products); 
                   })
                   ->get();
        $data = [
            'inprogressTasks' => $inprogressTasks,
            'lines'           => $lines,
            'role'            => $role,
        ];
        return View('layouts.dashboard.timer')->with($data);
    }


    /**
     * Show the Wallboards.
     *
     * @return \Illuminate\Http\Response
     */
    public function wallboards()
    {
        $role = auth()->user()->getRoles()->first()->slug;

        if($role == 'employee'){
            $inprogressTasks = Task::with('log','products')->where('user_id',auth()->user()->id)->whereIn('status',['Assigned','InProgress','Stopped'])->get();
        }else{
            $inprogressTasks = Task::with('log','products')->whereIn('status',['Assigned','InProgress','Stopped'])->get();
        }
        $products = [];
        foreach ($inprogressTasks as $key => $task) {
            $products = array_unique(array_merge($products,$task->products->pluck('id')->toarray()));
        }
        $stations = Station::with('products')
                   ->whereHas('products', function($q) use ($products){
                       $q->whereIn('product_id', $products); 
                   })
                   ->get();

        $users = User::with('roles')->whereHas('roles', function($query) {
                        $query->where('roles.id',3);
                    })->get();

        $data = [
            'tasks'           => $inprogressTasks,
            'stations'        => $stations,
            'users'           => $users,
            'role'            => $role,
        ];
        return View('layouts.dashboard.wallboards')->with($data);
    }

    public function updateTimer(Request $request)
    {
        $taskId = $request->id;
        $taskType = $request->type;

        $task = Task::find($taskId);
        $status = '';

        if($taskType == 'start' && ($task->status == 'Assigned' || $task->status == 'Stopped')){
            $status     = 'InProgress';
            $log = [new TaskLog(['started_by' => auth()->user()->id, 'start_time' => \Carbon\Carbon::now()])];
        }elseif($taskType == 'stop' && $task->status == 'InProgress'){
            $status     = 'Stopped';
            $tasklog = $task->log->last();
            $tasklog->end_by = auth()->user()->id;
            $tasklog->end_time = \Carbon\Carbon::now();
            $tasklog->save();
        }elseif($taskType == 'completed'){
            $status     = 'Completed';
            $force_complete = false;
            if($task->status == 'InProgress'){
                $tasklog = $task->log->last();
                $tasklog->end_by = auth()->user()->id;
                $tasklog->end_time = \Carbon\Carbon::now();
                $tasklog->save();
            }else if($task->status == 'Assigned'){
                $force_complete = true;
            }
            $this->updateUserBonus($taskId, $force_complete);
        }
        if($status != ''){
            $task->status   = $status;
            $task->save();

            if(isset($log)){
                $task->log()->saveMany($log);
            }
        }

        $data['status']  = 'true';
        return response()->json($data); 
    }

    public function updateUserBonus($taskId,$force_complete = false)
    {
        $task = Task::find($taskId);

        $taskDuration = ($task->products->first()->days *24*60*60)+($task->products->first()->hours*60*60)+($task->products->first()->minutes*60);
        $seconds = $userbonus = $totalDiff = 0;
        if(!$force_complete){
            if(count($task->log) > 0){
                foreach($task->log as $log){
                    $dateStart = \Carbon\Carbon::parse($log->start_time);
                    if(!empty($log->end_time)){
                        $dateEnd = \Carbon\Carbon::parse($log->end_time);
                    }else{
                        $dateEnd = \Carbon\Carbon::now();
                    }
                    $seconds += $dateStart->diffInSeconds($dateEnd);
                }
            }

            if($seconds > $taskDuration){
                $status = 'overdue';
                $totalDiff = $seconds - $taskDuration;
                if($task->bonus_applicable){
                    $userbonus = (-1)*($totalDiff * ($task->products->first()->bonus)/60);
                }
            }elseif($seconds < $taskDuration){
                $status = 'beforetime';
                $totalDiff = $taskDuration - $seconds;
                if($task->bonus_applicable){
                    $userbonus = ($totalDiff * ($task->products->first()->bonus)/60);
                }
            }else{
                $status = "beforetime";
            }
        }else{
            $status = "beforetime";
        }
        $task->usertask()->attach($task->user_id,['task_time' => $taskDuration, 'user_time' => $seconds,'status' => $status,'bonus' => $userbonus]);
    }
}
