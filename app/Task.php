<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

	/**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'user_id',
        'status',
        'bonus_applicable',
        'priority',
        'task_date'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function products()
    {
        return $this->belongsToMany('App\Product','task_products','task_id', 'product_id');
    }


    public function productionlines()
    {
        return $this->belongsToMany('App\ProductLine','task_production_lines','task_id','product_line_id');
    }

    public function log()
    {
        return $this->hasMany('App\TaskLog','task_id','id');
    }

    public function usertask()
    {
        return $this->belongsToMany('App\User','task_user_task','task_id','user_id')->withPivot('task_time', 'user_time','status','bonus');
    }

    public function stations()
    {
        return $this->belongsToMany('App\Station','task_stations','task_id','station_id');
    }

    public function getTaskDateAttribute($date)
    {
        return \Carbon\Carbon::parse($date)->format('m/d/Y');
    }
}
