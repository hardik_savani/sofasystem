<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskLog extends Model
{
    use SoftDeletes;

	/**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $dates = [
        'start_time',
        'end_time',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task_id',
        'started_by',
        'end_by',
        'start_time',
        'end_time'
    ];

    /**
     * Get the user that owns the phone.
     */
    public function task()
    {
        return $this->belongsTo('App\Task');
    }
}
