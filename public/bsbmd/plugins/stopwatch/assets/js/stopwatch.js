function updateTimer(id,type){
    return $.ajax({
        url: '/updateTimer',
        type: 'post',
        data: {_token: CSRF_TOKEN, id:id,type:type},
        dataType: 'JSON',
        success: function (data) {
        }
    });
}
var varsClock = {};
var halfTime  = 10*60*1000;
function initTimer(){
    localStorage.clear();
    varsClock = {};
    $(".container.stopwatch").each(function() {
        var clockNo = $(this).attr('data-id');
        var seconds = $(this).attr('data-sec') * 1000;
        var status  = $(this).attr('data-status');
        var duration = $(this).attr('data-duration') * 1000;

        // Stopwatch
        localStorage.setItem('stopwatchInterval' + clockNo,0);// The interval for our loop.
        varsClock['stopwatchClock' + clockNo] = $(".container.stopwatch"+clockNo).find(".clock"),
        varsClock['stopwatchDigits' + clockNo] = varsClock['stopwatchClock' + clockNo].find('span');

        localStorage.setItem('duration' + clockNo,duration);

        localStorage.setItem('stopwatchRunningTime' + clockNo,seconds);
        if(seconds != 0){
            varsClock['stopwatchDigits' + clockNo].text(returnFormattedToMilliseconds(Number(localStorage.getItem('stopwatchRunningTime' + clockNo))));
            if(status == 'InProgress'){
                startStopwatch(clockNo);
            }
        }

        if(parseInt(localStorage.getItem('stopwatchRunningTime' + clockNo)) > parseInt(localStorage.getItem('duration' + clockNo))){//over due
            $(".container.stopwatch"+clockNo).removeClass("bg-green bg-amber").addClass('bg-red');
        }else{
            if((parseInt(localStorage.getItem('duration' + clockNo)) - parseInt(localStorage.getItem('stopwatchRunningTime' + clockNo))) <= halfTime){
                $(".container.stopwatch"+clockNo).removeClass("bg-green bg-red").addClass('bg-amber');
            }
        }
    });
}
initTimer();
$('body').on('click','.stopwatch-btn-start',function(e){
    e.preventDefault();
    var clockNo = $(this).attr('data-id');
    if(varsClock['stopwatchClock' + clockNo].hasClass('inactive')){
        $.when(updateTimer(clockNo,'start')).done(function(a1){
            if(a1.status == 'true'){
                startStopwatch(clockNo);
            }
        });        
    }
});

$('body').on('click','.stopwatch-btn-complete',function(e){
    e.preventDefault();
    var clockNo = $(this).attr('data-id');
    $.when(updateTimer(clockNo,'completed')).done(function(a1){
        if(a1.status == 'true'){
            $(".stopwatch"+clockNo).parent().remove();
            showNotification('bg-green', 'Task Successfully Completed!', 'bottom', 'right', 'animated fadeInRight', 'animated fadeOutRight');
            /*alarmSound.currentTime = 0;
            alarmSound.play();
            setTimeout(function(){
                alarmSound.pause();
                alarmSound.currentTime = 0;
            }, 3800);*/
        }
    });
});
$('body').on('click','.stopwatch-wallboard-complete',function(e){
    e.preventDefault();
    var clockNo = $(this).attr('data-id');
    $.when(updateTimer(clockNo,'completed')).done(function(a1){
        if(a1.status == 'true'){
            $(".stopwatch"+clockNo).parent().remove();
            showNotification('bg-green', 'Task Successfully Completed!', 'top', 'right', 'animated fadeInRight', 'animated fadeOutRight');
            clearInterval(interval);
            getwallboardtask();
        }
    });
});

$('body').on('click','.stopwatch-btn-pause',function(e){
    e.preventDefault();
    var clockNo = $(this).attr('data-id');
    $.when(updateTimer(clockNo,'stop')).done(function(a1){
        if(a1.status == 'true'){
            pauseStopwatch(clockNo);
            console.log(clockNo);
        }
    });
});

$('body').on('click','.stopwatch-btn-reset',function(){
    resetStopwatch($(this).attr('data-id'));
});

// Pressing the clock will pause/unpause the stopwatch.
/*stopwatchClock.on('click',function(){

    if(stopwatchClock.hasClass('inactive')){
        startStopwatch()
    }
    else{
        pauseStopwatch();
    }
});*/

function startStopwatch(stopwatchNo){
    // Prevent multiple intervals going on at the same time.
    clearInterval(localStorage.getItem('stopwatchInterval' + stopwatchNo));

    var startTimestamp = new Date().getTime(),
        runningTime = 0;

    localStorage.setItem('stopwatchBeginingTimestamp' + stopwatchNo, startTimestamp);

    // The app remembers for how long the previous session was running.
    if(Number(localStorage.getItem('stopwatchRunningTime' + stopwatchNo))){
        runningTime = Number(localStorage.getItem('stopwatchRunningTime' + stopwatchNo));
    }
    else{
        localStorage.setItem('stopwatchRunningTime' + stopwatchNo,1)
    }

    // Every 100ms recalculate the running time, the formula is:
    // time = now - when you last started the clock + the previous running time
    localStorage.setItem('stopwatchInterval' + stopwatchNo,setInterval(function () {
        var stopwatchTime = (new Date().getTime() - startTimestamp + runningTime);

        varsClock['stopwatchDigits' + stopwatchNo].text(returnFormattedToMilliseconds(stopwatchTime));

        //check if remaining time is less then 10 minutes or over due
        if(parseInt(localStorage.getItem('stopwatchRunningTime' + stopwatchNo)) > parseInt(localStorage.getItem('duration' + stopwatchNo))){//over due
            $(".container.stopwatch"+stopwatchNo).removeClass("bg-green bg-amber").addClass('bg-red');
            //.fadeTo(300, 0.4, function() { $(this).fadeTo(100, 1.0); });
        }else{
            if((parseInt(localStorage.getItem('duration' + stopwatchNo)) - parseInt(localStorage.getItem('stopwatchRunningTime' + stopwatchNo))) <= halfTime){
                $(".container.stopwatch"+stopwatchNo).removeClass("bg-green bg-red").addClass('bg-amber');
                //.fadeTo(300, 0.4, function() { $(this).fadeTo(100, 1.0); });
            }
        }
    }, 100))

    varsClock['stopwatchClock' + stopwatchNo].removeClass('inactive');
}

function pauseStopwatch(stopwatchNo){
    // Stop the interval.
    clearInterval(localStorage.getItem('stopwatchInterval' + stopwatchNo));

    if(Number(localStorage.getItem('stopwatchBeginingTimestamp' + stopwatchNo))){

        // On pause recalculate the running time.
        // new running time = previous running time + now - the last time we started the clock.
        var runningTime = Number(localStorage.getItem('stopwatchRunningTime' + stopwatchNo)) + new Date().getTime() - Number(localStorage.getItem('stopwatchBeginingTimestamp' + stopwatchNo));

        localStorage.setItem('stopwatchBeginingTimestamp' + stopwatchNo, 0);
        localStorage.setItem('stopwatchRunningTime' + stopwatchNo,runningTime)

        varsClock['stopwatchClock' + stopwatchNo].addClass('inactive');
    }
}

// Reset everything.
function resetStopwatch(stopwatchNo){
    clearInterval(localStorage.getItem('stopwatchInterval' + stopwatchNo));

    varsClock['stopwatchDigits' + stopwatchNo].text(returnFormattedToMilliseconds(0));
    localStorage.setItem('stopwatchBeginingTimestamp' + stopwatchNo, 0);
    localStorage.setItem('stopwatchRunningTime' + stopwatchNo,0)
    varsClock['stopwatchClock' + stopwatchNo].addClass('inactive');
}


function returnFormattedToMilliseconds(time){
    var milliseconds = Math.floor((time % 1000) / 100),
        seconds = Math.floor((time/1000) % 60),
        minutes = Math.floor((time/(1000*60)) % 60),
        hours = Math.floor((time/(1000*60*60)) % 24);

    seconds = seconds < 10 ? '0' + seconds : seconds;
    minutes = minutes < 10 ? '0' + minutes : minutes;


    return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
}