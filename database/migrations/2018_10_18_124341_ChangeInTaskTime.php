<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeInTaskTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table){
            $table->dropColumn('days');
            $table->dropColumn('hours');
            $table->dropColumn('minutes');
        });
        Schema::table('products', function($table) {
            $table->integer('days')->nullable()->after('bonus');
            $table->integer('hours')->nullable()->after('days');
            $table->integer('minutes')->nullable()->after('hours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
