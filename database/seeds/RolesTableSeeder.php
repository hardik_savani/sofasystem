<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Role::where('name', '=', 'Admin')->first() === null) {
            $adminRole = Role::create([
                'name'        => 'Admin',
                'slug'        => 'admin',
                'description' => 'Admin Role',
                'level'       => 5,
            ]);
        }

        if (Role::where('name', '=', 'Supervisors')->first() === null) {
            $userRole = Role::create([
                'name'        => 'Supervisors',
                'slug'        => 'supervisor',
                'description' => 'Supervisors Role',
                'level'       => 4,
            ]);
        }

        if (Role::where('name', '=', 'Employee')->first() === null) {
            $userRole = Role::create([
                'name'        => 'Employee',
                'slug'        => 'employee',
                'description' => 'Employee Role',
                'level'       => 3,
            ]);
        }
    }
}
