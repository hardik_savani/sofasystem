<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::group(['middleware'=>'auth'],function(){
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/',['as'=>'dashboard','uses'=>'DashboardController@index']);	
    Route::get('timer',['as'=>'timer','uses'=>'DashboardController@timer']);
    Route::get('wallboards',['as'=>'wallboards','uses'=>'DashboardController@wallboards']);
    Route::post('updateTimer',['as'=>'updateTimer','uses'=>'DashboardController@updateTimer']);
    
    Route::resource('/users/deleted', 'SoftDeletesController', [
        'only' => [
            'index', 'show', 'update', 'destroy',
        ],
    ]);
    Route::resource('users', 'UsersManagementController', [
        'names'    => [
            'index'   => 'users',
            'create'  => 'create',
            'destroy' => 'user.destroy',
        ],
        'except' => [
            'deleted',
        ],
    ]);

    Route::resource('products', 'ProductsManagementController', [
        'names'    => [
            'index'   => 'products',
            'create'  => 'create',
            'destroy' => 'product.destroy',
        ],
        'except' => [
            'deleted',
        ],
    ]);

    Route::resource('productionionlines', 'ProductLinesManagementController', [
        'names'    => [
            'index'   => 'productionionlines',
            'create'  => 'create',
            'destroy' => 'productionionline.destroy',
        ],
        'except' => [
            'deleted',
        ],
    ]);

    Route::resource('tasks', 'TaskController', [
        'names'    => [
            'index'   => 'tasks',
            'create'  => 'create',
            'destroy' => 'task.destroy',
        ],
        'except' => [
            'deleted',
        ],
    ]);

    Route::resource('stations', 'StationController', [
        'names'    => [
            'index'   => 'stations',
            'create'  => 'create',
            'destroy' => 'stations.destroy',
        ],
        'except' => [
            'deleted',
        ],
    ]);
	
    Route::resource('attributes', 'AttributeController', [
        'names'    => [
            'index'   => 'attributes',
            'create'  => 'create',
            'destroy' => 'attribute.destroy',
        ],
        'except' => [
            'deleted',
        ],
    ]);

    Route::post('/getproductionlist','TaskController@getproductionlist');
    Route::get('/getdashboardtask','TaskController@getdashboardtask');
    Route::post('/getstationlist','TaskController@getstationlist');
    Route::post('/getstationbylines','TaskController@getstationbylines');
    Route::post('/gettasklist','TaskController@gettasklist');
    Route::post('/getwallboardtask','TaskController@getwallboardtask');
    Route::get('/getproduct','TaskController@getproduct');
    Route::get('reorder','TaskController@reorder');
    Route::post('getusertasklist','TaskController@getusertasklist');
    Route::post('updateusertaskorder','TaskController@updateusertaskorder');
});